import React, { Suspense } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Testpage from './pages/Testpage/page';
import DesktopSix from './pages/DesktopSix';
import DesktopSeven from './pages/DesktopSeven';
import DesktopEleven from './pages/DesktopEleven';
import NotFound from './pages/NotFound';
import Settings from './pages/Settings';
import DesktopTwenty from './pages/DesktopTwenty';
import SignInPage from './pages/login';
import DesktopTwentyOne from 'pages/DesktopTwentyOne';
import NewLogin from 'pages/NewLogin';
import DesktopTwentyTwo from 'pages/DesktopTwentyTwo';
import DesktopTwentyThree from 'pages/DesktopTwentyThree';
import DashboardHome from 'pages/DashboardHome';
import Dashboard1 from 'pages/LiveDashboard';
import LiveDashboard from 'pages/LiveDashboard';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<NewLogin />} />
        <Route path="/Testpage" element={<Testpage />} />
        <Route path="/DesktopTwenty" element={<DesktopTwenty />} />
        <Route path="/DesktopTwentyTwo" element={<DesktopTwentyTwo />} />
        <Route path="/DesktopTwentyOne" element={<DesktopTwentyOne />} />
        <Route path="/DesktopTwentyThree" element={<DesktopTwentyThree />} />
        <Route path="/DesktopSix" element={<DesktopSix />} />
        <Route path="/desktopseven" element={<DesktopSeven />} />
        <Route path="/desktopeleven" element={<DesktopEleven />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/Settings" element={<Settings />} />
        <Route path="/NewLogin" element={<NewLogin />} />
        <Route path="/Dashboard" element={<DashboardHome />} />
        <Route path="/Dashlive" element={<LiveDashboard />} />
      </Routes>
    </Router>
  );
}

export default App;