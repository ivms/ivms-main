import React, { useState } from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css'; // Import Font Awesome CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './RadioButtons.css';
import { Line } from '../Line';


const RadioButtons = () => {
  const [selectedOption, setSelectedOption] = useState(null);
  const [showAdditionalOptions, setShowAdditionalOptions] = useState(false);
  const [selectedAdditionalOptions, setSelectedAdditionalOptions] = useState([]);

  const handleOptionChange = (value) => {
    setSelectedOption(value);
    setShowAdditionalOptions(true);
  };

  const handleAdditionalOptionChange = (value) => {
    const updatedSelection = selectedAdditionalOptions.includes(value)
      ? selectedAdditionalOptions.filter((option) => option !== value)
      : [...selectedAdditionalOptions, value];

    setSelectedAdditionalOptions(updatedSelection);

    console.log(`Selected additional options: ${updatedSelection.join(', ')}`);
  };

  const optionImages = {
    Manufacturing: '/images/manufaturing.png',
    Shipping: '/images/shipping.png',
    RoadTransport: '/images/RoadTransport.png',
    Airline: '/images/Airline.png',
  };
  
  const additionalOptionImages = {
    Helmet: '/images/Helmet.png',
    SafetyGlass: '/images/Glass.png',
    Gloves: '/images/Glove.png',
    Mask: '/images/mask.png',
  };

  const additionalOptions = ['Helmet', 'SafetyGlass', 'Gloves', 'Mask'];

  const optionImageStyles = {
    Manufacturing: { maxWidth: '24%', maxHeight: '24%', marginRight: '8px' },
    Shipping: { maxWidth: '25%', maxHeight: '25%', marginRight: '8px' },
    RoadTransport: { maxWidth: '27%', maxHeight: '27%', marginRight: '8px' },
    Airline: { maxWidth: '25%', maxHeight: '25%', marginRight: '8px' },
  };

  const additionalOptionImageStyles = {
    Helmet: { maxWidth: '25%', maxHeight: '27%', marginRight: '8px' },
    SafetyGlass: { maxWidth: '25%', maxHeight: '27%', marginRight: '8px' },
    Gloves: { maxWidth: '27%', maxHeight: '28%', marginRight: '8px' },
    Mask: { maxWidth: '24%', maxHeight: '24%', marginRight: '8px' },
  };

  return (
    <div>
      <div className="row">
        {Object.keys(optionImages).map((option, index) => (
          <div key={index} className="col-md-3">
            <div
              className={`rounded-box ${selectedOption === option ? 'selected' : ''}`}
              onClick={() => handleOptionChange(option)}
              style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginBottom: '16px' }}
            >
              <img
                src={optionImages[option]}
                alt={option}
                style={optionImageStyles[option]}
              />
              {option}
            </div>
          </div>
        ))}
      </div>

      {showAdditionalOptions && (
        <div className="row">
           <text style={{ fontWeight: 'bold', marginBottom: '30px' }}>
            
          3.  SELECT FUNCTIONS
          </text>
          <Line className="bg-white h-px mr-[3px] mt-[0.1px] w-full" />
          <text style={{ fontWeight: 'bold ', fontStyle: "oblique" }}>
          Please select Functions:
          </text>
          <Line className="bg-white h-px mr-[3px] mt-[15px] w-full" />
          {additionalOptions.map((option, index) => (
            <div key={index} className="col-md-3">
              <div
                className={`rounded-box ${
                  selectedAdditionalOptions.includes(option) ? 'selected' : ''
                }`}
                onClick={() => handleAdditionalOptionChange(option)}
                style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', marginBottom: '16px' }}
              >
                <img
                  src={additionalOptionImages[option]}
                  alt={option}
                  style={additionalOptionImageStyles[option]}
                />
                {option}
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default RadioButtons;