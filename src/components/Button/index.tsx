import React from "react";

const shapes = { round: "rounded-[9px]" } as const;
const variants = {
  fill: {
    white_A700: "bg-white-A700 shadow-bs text-indigo-A700_02",
    indigo_A700_02: "bg-indigo-A700_02 shadow-bs text-white-A700",
    gray_50: "bg-gray-50 text-gray-500",
  },
  gradient: {
    indigo_A700_01_indigo_900: "bg-gradient  shadow-bs2 text-white-A700",
  },
} as const;
const sizes = { xs: "py-px", sm: "p-[5px]", md: "p-3" } as const;

export type ButtonProps = Omit<
  React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >,
  "onClick"
> &
  Partial<{
    className: string;
    shape: keyof typeof shapes;
    variant: keyof typeof variants;
    size: keyof typeof sizes;
    color: string;
    leftIcon: React.ReactNode;
    rightIcon: React.ReactNode;
    onClick: () => void;
  }>;

const Button: React.FC<React.PropsWithChildren<ButtonProps>> = ({
  children,
  className = "",
  leftIcon,
  rightIcon,
  shape = "",
  size = "",
  variant = "",
  color = "",
  ...restProps
}) => {
  return (
    <button
      className={`${className} ${(shape && shapes[shape]) || ""} ${
        (size && sizes[size]) || ""
      } ${(variant && variants[variant]?.[color]) || ""}`}
      {...restProps}
    >
      {!!leftIcon && leftIcon}
      {children}
      {!!rightIcon && rightIcon}
    </button>
  );
};

export { Button };
