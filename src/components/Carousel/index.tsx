// Carousel.tsx

import React from "react";
import Slider, { Settings } from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { Img, LoginText } from "components";
import '../Carousel/styles.scss';

interface SlideProps {
  imagePath: string;
  title: string;
  content: string;
  content2: string;
  content3: string;
}

const slides: SlideProps[] = [
  {
    imagePath: "images/login/slider1.png",
    title: "OUR SERVICES",
    content: "License Plate Recognition",
    content2:
      "Vehicle license plate recognition, car make, model, colour and direction recognition.",
    content3: "01/03",
  },
  {
    imagePath: "images/login/slider1.png",
    title: "OUR SERVICES",
    content: "Face Recognition",
    content2:
      "Face detection and comparison with the predefined lists of face. Estimates the age and gender of the detected person.",
    content3: "02/03",
  },
  {
    imagePath: "images/login/slider1.png",
    title: "OUR SERVICES",
    content: "Smoke and Fire Detection",
    content2:
      "Early detection of smoke and fire",
    content3: "03/03",
  },
  // Add more slides as needed
];

const NextArrow: React.FC<{ onClick?: () => void }> = ({ onClick }) => (
  <button className="custom-arrow next" onClick={onClick}>
  </button>
);

const PrevArrow: React.FC<{ onClick?: () => void }> = ({ onClick }) => (
  <button className="custom-arrow prev" onClick={onClick}>
  </button>
);

const Carousel: React.FC = () => {
  const settings: Settings = {
    fade: true,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  };

  return (
    <Slider {...settings}>
      {slides.map((slide, index) => (
        <div key={index}>
          <Grid container style={{ height: '100%' }}>
            <Grid item xs={12} sm={6} md={6}>
              <Img src={slide.imagePath} alt={slide.title} />
            </Grid>
            <Grid item xs={12} sm={6} md={6} style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
              <div className="carousel-content w-full p-5">
                <LoginText
                  className="text-[55px] w-[50%] mb-4"
                  size="txtMontserratExtraBold152"
                >
                  <Box className="leading-[1]" style={{ wordBreak: "break-word" }}>{slide.title}</Box>
                </LoginText>
                <LoginText
                  className="text-[18px] leading-10"
                  size="txtMontserratBold20"
                >
                  {slide.content}
                </LoginText>
                <LoginText
                  className="text-[15px] mt-2"
                  size="txtMontserratMedium40"
                >
                  {slide.content2}
                </LoginText>
                <LoginText size="txtMontserratExtraBold152" className="text-[50px] mt-[60px]">{slide.content3}</LoginText>
              </div>
            </Grid>
          </Grid>
        </div>
      ))}
    </Slider>
  );
};

export { Carousel };
