import React, { useEffect, useState } from "react";
  import { useNavigate } from "react-router-dom";
  import { Button, Img } from "../../components";
  import "./dashbord.css"; 

  type DashboardProps = React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > &
    Partial<{}>;

  const Dashboard: React.FC<DashboardProps> = (props) => {
    const navigate = useNavigate();
    const [isDashboardHovered, setIsDashboardHovered] = useState(false);
    const [isCameraConfigHovered, setIsCameraConfigHovered] = useState(false);
    const [isSettingsHovered, setIsSettingsHovered] = useState(false);
    const [isUserHovered, setIsUserHovered] = useState(false);
    const [isLogoutHovered, setIsLogoutHovered] = useState(false); // Declare isLogoutHovered state
    const [userPrivileges, setUserPrivileges] = useState([]);

  useEffect(() => {
    // Fetch user privileges from local storage
    const storedUserPrivileges = JSON.parse(localStorage.getItem('userPrivileges')) || [];
    setUserPrivileges(storedUserPrivileges);
  }, []);

  const handleDashboardButtonClick = () => {
    // Redirect logic based on user privileges
    if (userPrivileges.includes('Alpr detection')) {
      navigate('/desktoptwenty');
    } else if (userPrivileges.includes('Human detection')) {
      navigate('/desktoptwentyone');
    } else if (userPrivileges.includes('helmet detection')) {
      navigate('/desktoptwentythree');
    } else if (userPrivileges.includes('Cup detection')) {
      navigate('/DesktopTwentyTwo');
    }
  };


    return (
      <div className="max-height-wrapper">
      <div className="flex flex-wrap justify-center items-center space-y-4 md:space-y-0 md:space-x-4">
      <div className={props.className} style={{ overflow: "hidden" }}>
     

 <Button 
       style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "95px", // Set the desired width
          height: "80px", // Set the desired height
          top: "0", // Position the button at the top
          position: "absolute", 
        }}
      
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
       
      >
       
       <Img
      className="absolute w-full md:w-3/5 left-1/4 md:left-[45px] md:right-[30px] top-[2%] object-cover"
      src="images/img_group67.svg"
      alt="groupSixtySeven"
    />
          
           
          
       
      </Button>
     
    


      <Button 
       style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "45px", // Set the desired width
          height: "45px", // Set the desired height
       
         
        }}
        onMouseOver={(e) => {
          e.currentTarget.style.filter = "brightness(100%)";
          setIsDashboardHovered(true);
        }}
        onMouseOut={(e) => {
          e.currentTarget.style.filter = "brightness(50%)";
          setIsDashboardHovered(false);
        }}
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
        onClick={handleDashboardButtonClick}
      >
       
          <Img
            src="images/img_info.png"
            alt="info"
            className="w-[50px] h-[50px] "  // Set the desired size
          />
          {isDashboardHovered && (
            <div
              style={{
                position: "absolute",
                bottom: "-20px", // Adjust this value to control the distance from the image
                left: "50%",
                transform: "translateX(-50%)",
                textAlign: "center",
              }}
              className="font-montserrat leading-[normal] text-indigo-A700 text-sm"
            >
              Dashboard
            </div>
          )}
       
      </Button>

      <div style={{ margin: "1px 0" }} /> {/* Space between buttons */}

      <Button
         style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "50px", // Set the desired width
          height: "45px", // Set the desired height
        }}
        onMouseOver={(e) => {
          e.currentTarget.style.filter = "brightness(100%)";
          setIsCameraConfigHovered(true);
        }}
        onMouseOut={(e) => {
          e.currentTarget.style.filter = "brightness(50%)";
          setIsCameraConfigHovered(false);
        }}
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
        onClick={() => navigate("/DesktopSix")}
      >
        <Img
          src="images/img_group61.png"
          alt="groupSixtyOne"
          className="w-full h-full"
        />
        {isCameraConfigHovered && (
          <div
            style={{
              position: "absolute",
              bottom: "-35px",
              left: "50%",
              transform: "translateX(-50%)",
              textAlign: "center",
            }}
            className="font-montserrat leading-[normal] text-indigo-A700 text-sm"
          >
            Camera Config
          </div>
        )}
      </Button>
      <div style={{ margin: "1px 0" }} /> 
      <Button
        style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "55px", // Set the desired width
          height: "55px", // Set the desired height
        }}
        onMouseOver={(e) => {
          e.currentTarget.style.filter = "brightness(100%)";
          setIsSettingsHovered(true);
        }}
        onMouseOut={(e) => {
          e.currentTarget.style.filter = "brightness(50%)";
          setIsSettingsHovered(false);
        }}
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
        onClick={() => navigate("/settings")}
      >
        <Img
          src="images/img_settings.png"
          alt="settings"
          className="w-full h-full"
        />
        {isSettingsHovered && (
          <div
            style={{
              position: "absolute",
              bottom: "-20px",
              left: "50%",
              transform: "translateX(-50%)",
              textAlign: "center",
            }}
            className="font-montserrat leading-[normal] text-indigo-A700 text-sm"
          >
            Settings
          </div>
        )}
      </Button>
      <div style={{ margin: "1px 0" }} /> 

      <Button
        style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "75px", // Set the desired width
          height: "55px", // Set the desired height
        }}
        onMouseOver={(e) => {
          e.currentTarget.style.filter = "brightness(100%)";
          setIsUserHovered(true);
        }}
        onMouseOut={(e) => {
          e.currentTarget.style.filter = "brightness(50%)";
          setIsUserHovered(false);
        }}
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
        onClick={() => navigate("/settings")}
      >
        <Img
          src="images/img_user.png"
          alt="user"
          className="w-full h-full"
        />
        {isUserHovered && (
          <div
            style={{
              position: "absolute",
              bottom: "-20px",
              left: "50%",
              transform: "translateX(-50%)",
              textAlign: "center",
            }}
            className="font-montserrat leading-[normal] text-indigo-A700 text-sm"
          >
            User
          </div>
        )}
        
      </Button>
      <div style={{ margin: "2px 0" }} /> 
      <Button
        style={{
          filter: "brightness(100%)",
          transition: "filter 0.01s",
          width: "65px", // Set the desired width
          height: "55px", // Set the desired height
        }}
      
        className="common-pointer bg-transparent cursor-pointer flex flex-col h-[90px] items-center justify-center relative w-[60px]"
        onClick={() => navigate("/Testpage")}
      >
             <Img src="images/test_img.png" alt="user" className="w-full h-full" />
       
        
      </Button>
    

     
        <div style={{ margin: "1px 0" }} /> 

        {/* Logout Button */}
        <Button
          style={{
            filter: "brightness(100%)",
            transition: "filter 0.01s",
            width: "85px", // Set the desired width
            height: "65px", // Set the desired height
          }}
          onMouseOver={(e) => {
            e.currentTarget.style.filter = "brightness(100%)";
            setIsLogoutHovered(true);
          }}
          onMouseOut={(e) => {
            e.currentTarget.style.filter = "brightness(50%)";
            setIsLogoutHovered(false);
          }}
          className="bg-transparent cursor-pointer flex items-center justify-center min-w-[68px] mt-1" // Adjust the margin-top value
          onClick={() => navigate("/")}
        >
          <Img
            className="h-3 mt-0.5 mb-px mr-1.5"
            src="images/img_refresh.svg"
            alt="refresh"
          />
          <div
            className={`font-montserrat leading-[normal] text-left text-sm ${
              isLogoutHovered ? "text-blue-500" : "text-blue_gray-900"
            }`}
          >
            Logout
          </div>
        </Button>
      </div>
    </div>
    </div>
  );
};

Dashboard.defaultProps = {};
export default Dashboard;
