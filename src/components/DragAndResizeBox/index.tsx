import React, { useState } from "react";
import ReactLassoSelect from "react-lasso-select";
import Slider from "@mui/material/Slider";


import { useNavigate } from "react-router-dom";
import BASE_URL from "../../apiConfig";
import { Line } from "../../components/Line";

interface Point {
  x: number;
  y: number;
}

interface DragAndResizeBoxProps {
  videoSrc: string;
}

function pointsToString(points: Point[]): string {
  return points.map(({ x, y }) => `${x},${y}`).join(" ");
}


const DragAndResizeBox: React.FC<DragAndResizeBoxProps> = ({ videoSrc }) => {

  const init = "172,173 509,99 458,263"
    .split(" ")
    .map((c) => c.split(",").map(Number))
    .map(([x, y]) => ({ x, y }));
  const [points, setPoints] = useState<Point[]>(init);
  const resultPoints = pointsToString(points);

  const navigate = useNavigate(); // Move the useNavigate hook inside the component
  const handleSaveAndNavigate = async () => {
    await saveToJsonFile(); // Wait for the saveToJsonFile function to complete
    navigate("/desktopeleven");
  };
  const resetPoints = () => {
    setPoints(init);
    navigate("/desktopseven");
  }; 

  const saveToJsonFile = async () => {
    const xPoints = points.map(point => point.x);
    const yPoints = points.map(point => point.y);

const data = {
    x: xPoints,
    y: yPoints,
  };

    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };

    try {
      const response = await fetch(`${BASE_URL}/roi-settings/`, requestOptions);
      const responseData = await response.json();

      // Handle the API response here if needed
      console.log('API Response:', responseData);
    } catch (error) {
      console.error('Error sending data to API:', error);
    }
  };

  return (
    <div className="App" style={{ color: "#000", width: "100%", display: "flex", gap: '2rem' }}>
    <div>
      <ReactLassoSelect
        value={points}
        src={videoSrc}
        onChange={(path: Point[]) => {
          setPoints(path);
        }}
      />
      <br />
      Points:
      <div style={{ display: "flex" }}>{pointsToString(points)}</div>
      <br />
      <button
  className="common-pointer cursor-pointer font-semibold min-w-[243px] text-base text-center bg-indigo-900 text-white h-12 rounded-[15px]"
  onClick={handleSaveAndNavigate} // Call the combined function
  style={{ marginRight: '10px' }} // Adding margin-bottom for spacing
>
  Save ROI
</button>

{/* Adding margin-bottom for spacing */}
<button
  className="common-pointer cursor-pointer font-semibold min-w-[243px] text-base text-center bg-indigo-900 text-white h-12 rounded-[15px]"
  onClick={resetPoints}
  style={{ marginRight: '10px' }} // Adding margin-bottom for spacing
>
  Reset ROI
</button>

      </div>
      <div>
        
      </div>
    </div>
  );
};
export default DragAndResizeBox;