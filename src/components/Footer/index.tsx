// Footer.tsx
import React from "react";
import { Container, Grid, Typography } from "@mui/material";
import { Img, LoginText } from "components";
import Button from "@mui/material/Button";
import PhoneIcon from '@mui/icons-material/Phone';
import MailIcon from '@mui/icons-material/Mail';

const navItems = ["Home", "About", "Contact Us", "Download APP"];

const Footer: React.FC = () => {
  return (
    <footer className="py-5">
      <Container>
        <Grid container>
          <Grid item xs={12} sm={3} className="">
            <Img className="h-12" src="images/login/logo.png" alt="logo" />
            <Typography variant="body2">
              <LoginText
                className="text-dark leading-[2] text-[14px] my-2"
                size="txtMontserratMedium40"
              >
               
              </LoginText>
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
            sm={3}
            className="grid justify-center"
          >
            {navItems.map((item) => (
              <div>
                <Button
                  className=" mx-3 block"
                  key={item}
                  sx={{
                    color: "#000",
                    fontWeight: "bold",
                    textTransform: "capitalize",
                  }}
                >
                  {item}
                </Button>
              </div>
            ))}
          </Grid>
          <Grid item xs={12} sm={6} className="flex justify-end">
            <Grid
              container
              spacing={{ xs: 2, md: 3 }}
              columns={{ xs: 3, sm: 8, md: 12 }}
            >
              <Grid item xs={12} sm={3} md={3}>
                <Img
                  className="ml-auto"
                  src="images/login/instagram.png"
                  alt="instagram"
                  style={{ width: '40px' }}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3} >
                <Img
                  className="ml-auto"
                  src="images/login/whatsapp.png"
                  alt="whatsapp"
                  style={{ width: '40px' }}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3}>
                <Img
                  className="ml-auto"
                  src="images/login/facebook.png"
                  alt="facebook"
                  style={{ width: '40px' }}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3}>
                <Img
                  className="ml-auto"
                  src="images/login/twitter.png"
                  alt="twitter"
                  style={{ width: '40px' }}
                />
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
                <Grid className="text-right"><PhoneIcon /> +91 9947217142</Grid>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>
                <Grid className="text-right"><MailIcon /> vibot@gmail.com</Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </footer>
  );
};

export { Footer };
