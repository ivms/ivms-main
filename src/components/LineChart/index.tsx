// Import necessary libraries
import { ApexOptions } from 'apexcharts';
import React from 'react';
import ReactApexChart from 'react-apexcharts';

// Define the Chart component
const LineChart = ({ data }) => {
// Inside the LineChart component

const options = {
  chart: {
    type: 'line',
  },
  xaxis: {
    categories: data.labels,
  },
  stroke: {
    curve: 'smooth',
  },
} as ApexOptions; // Add this line to explicitly specify the type

// Rest of the component code...


  return (
    <ReactApexChart
      options={options}
      series={data.datasets}
      type="line"
      height={350}
      width={550}
    />
  );
};

export default LineChart;