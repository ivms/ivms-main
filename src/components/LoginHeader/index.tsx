import React, { useState } from "react";
import { LoginButton, Img, LoginInput, LoginText } from 'components';
import BASE_URL from "apiConfig";
import FormControlLabel from "@mui/material/FormControlLabel";
import TextField from "@mui/material/TextField";
import OutlinedInput from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { ThemeProvider, createTheme } from '@mui/material/styles';

// const headerImg =  require("images/login/header.png");

const theme = createTheme({
  components: {
    // Name of the component
    MuiOutlinedInput: {
      styleOverrides: {
        // Name of the slot
        root: {
          // Some CSS
          color: '#ffffff',
          border: '1px solid #ffffff',
          "&::placeholder": {
            color: "#ffffff"
          },
        },
      },
    },
  },
});
const LoginHeader: React.FC = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [userPrivileges, setUserPrivileges] = useState([]);
  
  const redirectToDesktopTwenty = () => {
    // Implement your custom redirection logic for 'alpr'
    // For example:
    window.location.href = '/desktoptwenty';
  };
  
  const redirectToDesktopTwentyOne = () => {
    // Implement your custom redirection logic for 'Human detection'
    // For example:
    window.location.href = '/desktoptwentyone';
  };
  
  const redirectToDesktopTwentyThree = () => {
    // Implement your custom redirection logic for 'helmet detection'
    // For example:
    window.location.href = '/desktoptwentythree';
  };
  
  const redirectToDesktopTwentyTwo = () => {
    // Implement your custom redirection logic for 'Cup detection'
    // For example:
    window.location.href = '/DesktopTwentyTwo';
  };


  const handleSignIn = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(`${BASE_URL}/signin/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      });
    
      if (response.ok) {
        const userData = await response.json();
        console.log("Post Success - UserData:", userData);
        
        const userPrivileges = userData.privilege || [];
        localStorage.setItem('userPrivileges', JSON.stringify(userPrivileges));
        
        // Log stored user privileges
        const storedUserPrivileges = localStorage.getItem('userPrivileges');
        console.log('Stored User Privileges:', storedUserPrivileges);
        
        setUserPrivileges(userPrivileges);
        
        // Redirect based on user privileges
        if (userPrivileges.includes('Alpr detection')) {
          redirectToDesktopTwenty();
        } else if (userPrivileges.includes('Human detection')) {
          redirectToDesktopTwentyOne();
        } else if (userPrivileges.includes('helmet detection')) {
          redirectToDesktopTwentyThree();
        } else if (userPrivileges.includes('Cup detection')) {
          redirectToDesktopTwentyTwo();
        }
      }
    } catch (error) {
      // Handle any exceptions that occur within the try block
      console.error("Error:", error);
    } finally {
      // Code to execute whether there's an error or not (optional)
    }
    
      
  
      
};

  return (

    <div className="mx-auto md:px-5 relative w-full">
      <div className="w-[100%] md:w-full">
        <Img
          className="h-[95vh] m-auto object-cover w-full px-5"
          src="images/login/header.png"
          alt="maskgroupThree"
        />
        <div className="absolute bg-cover bg-no-repeat bottom-[0] flex flex-col h-[626px] items-start justify-end left-[0] p-[74px] md:px-10 sm:px-5 w-[74%] top-[4%]">
          <div className="flex flex-col items-start justify-start ml-2.5 md:ml-[0] mt-[100px]">
            <LoginText
              className="leading-[49.00px] font-bold	 sm:text-4xl md:text-[38px] text-[30px] text-white-A700 tracking-[0.24px]"
              size="txtMontserratBold20"
            >
              <>
              Beacon Vbot
                <br />
               
              </>
            </LoginText>
            <LoginText
              className="leading-[29.00px] text-lg text-[17px] text-white-A700 tracking-[1.26px] w-[83%] sm:w-full"
              size="txtMontserratRegular18"
            >
             Vbot is an all-in-one, cross-platform solution that seamlessly combines Video Management, Surveillance, and Video Analytics services. It simplifies camera and NVR management across multiple locations, offering a unified space for control. Easily add cameras and use cases at your convenience, all within a single, user-friendly interface. Experience the benefits of configurable alert systems for improved security and operational efficiency.
            </LoginText>
          </div>
        </div>
      </div>
      <div id="loginBox"
        className="w-[40%] sm:w-[100%] relative bg-cover bg-no-repeat flex flex-col top-[50%] md:top-[60%] sx:top-[50%] items-center justify-start lg:mt-20 px-[50px] py-[40px] md:px-10 sm:px-5 right-[0] sx:h-[50vh]"
        style={{
          background:
            "transparent linear-gradient(138deg, #001fd6 0%, #19317B 100%) 0% 0% no-repeat padding-box ",
          margin: "-20% 0 0 auto",
        }}
      >
        <div className="flex flex-col items-start  mb-[17px] mt-2 w-full">
          <LoginText
            className="leading-[50.00px] text-[35px] sm:text-[30px] md:text-[30px] lg:text-[30px]  text-white-A700 font-weight-bold"
            size="txtMontserratBold20"
          >
            <>
              We Are Responsible
              <br />
              For Your Growth
            </>
          </LoginText>
          <LoginText
            className="text-lg text-white-A700 tracking-[1.26px]"
            size="txtMontserratLight18"
          >
           
          </LoginText>
          {/* <LoginInput
          name="group1436"
          placeholder="Username"
          className="font-semibold p-0 placeholder:text-white-A700 text-base text-left w-full"
          wrapClassName="mt-7 w-full"
          type="text"
        ></LoginInput>
        <LoginInput
          name="group1437"
          placeholder="Password"
          className="font-semibold p-0 placeholder:text-white-A700 text-base text-left w-full"
          wrapClassName="mt-[33px] w-full"
          type="password"
        ></LoginInput>
        <LoginButton
          className="cursor-pointer font-bold min-w-[218px] md:ml-[0] text-center text-xl tracking-[0.60px] mt-5"
          size="sm"
          variant="outline"
        >
          LOGIN
        </LoginButton> */}
          <Box component="form" className="text-right" onSubmit={handleSignIn} sx={{ mt: 1 }}>
            <ThemeProvider theme={theme}>
              <OutlinedInput
                margin="normal"
                className="bg-transparent text-white-A700"
                required
                fullWidth
                id="username"
                placeholder="Username"
                name="username"
                autoComplete="username"
                autoFocus
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                sx={{ color: '#ffffff' }}
              />
              <OutlinedInput
                margin="normal"
                className="bg-transparent"
                required
                fullWidth
                name="password"
                placeholder="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </ThemeProvider>

            <LoginButton
              type="submit"
              className="cursor-pointer font-bold min-w-[218px] md:ml-[0] text-center text-xl tracking-[0.60px] mt-5"
              size="sm"
              variant="outline"
            >
              Sign In
            </LoginButton>
          </Box>
        </div>
      </div>
    </div>
  );
};

export { LoginHeader };
