import React from "react";

const sizeClasses = {
  txtMontserratLight18: "font-light font-montserrat",
  txtMontserratBold14: "font-bold font-montserrat",
  txtMontserratBold20: "font-bold font-montserrat",
  txtMontserratExtraBold90: "font-extrabold font-montserrat",
  txtMontserratSemiBold39: "font-montserrat font-semibold",
  txtMontserratSemiBold25: "font-montserrat font-semibold",
  txtMontserratRegular18Gray900: "font-montserrat font-normal",
  txtMontserratRegular18: "font-montserrat font-normal",
  txtMontserratExtraBold55: "font-extrabold font-montserrat",
  txtMontserratSemiBold20: "font-montserrat font-semibold",
  txtMontserratExtraBold152: "font-extrabold font-montserrat",
  txtMontserratExtraBold70: "font-extrabold font-montserrat",
  txtMontserratExtraBold74: "font-extrabold font-montserrat",
  txtMontserratSemiBold25WhiteA700: "font-montserrat font-semibold",
  txtMontserratMedium40: "font-medium font-montserrat",
} as const;

export type TextProps = Partial<{
  className: string;
  size: keyof typeof sizeClasses;
  as: any;
}> &
  React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLSpanElement>,
    HTMLSpanElement
  >;

const LoginText: React.FC<React.PropsWithChildren<TextProps>> = ({
  children,
  className = "",
  size,
  as,
  ...restProps
}) => {
  const Component = as || "p";

  return (
    <Component
      className={`text-left ${className} ${size && sizeClasses[size]}`}
      {...restProps}
    >
      {children}
    </Component>
  );
};

export { LoginText };
