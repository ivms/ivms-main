import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import Avatar from "@mui/material/Avatar";
import { Img, LoginButton } from "components";
import { Link as ScrollLink, animateScroll as scroll } from 'react-scroll';

// const logo =  require("../../assets/images/login/logo.png");

interface Props {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window?: () => Window;
}

const drawerWidth = 240;
const navItems = ["Home", "About", "Contact Us"];

export default function DrawerAppBar(props: Props) {
  const scrollToSection = (section: string) => {
    scroll.scrollTo(section, {
      duration: 100,
      offset: -10, // Adjust the offset as needed
      smooth: 'easeInOut',
    });
  };
  const { window } = props;
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        <Img className="h-12" src="images/login/logo.png" alt="logo" />
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item} disablePadding>
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary={item} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar component="nav" sx={{ backgroundColor: "#fff", px: 3 }}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: "none" } }}
          >
            <MenuIcon />
          </IconButton>
          <Tooltip title="Instaandon">
            <IconButton sx={{ p: 0 }}>
              <Img className="h-12" src="images/login/logo.png" alt="logo" />
            </IconButton>
          </Tooltip>
          <Box
            sx={{ display: { xs: "none", sm: "block" }, margin: "0 0 0 auto" }}
          >

            {/* {navItems.map((item) => ( */}
            <Button className="uppercase mx-3" sx={{ color: "#000", fontWeight: "bold" }}>
              <ScrollLink style={{ color: '#000' }}
                activeClass="active"
                to="home" // Replace with the ID of the section you want to scroll to
                spy={true}
                smooth={true}
                duration={800}
                offset={-50} // Adjust the offset as needed
              >Home</ScrollLink>
            </Button>
            <Button className="uppercase mx-3" sx={{ color: "#000", fontWeight: "bold" }}>
              <ScrollLink style={{ color: '#000' }}
                activeClass="active"
                to="about" // Replace with the ID of the section you want to scroll to
                spy={true}
                smooth={true}
                duration={800}
                offset={-50} // Adjust the offset as needed
              >About Us</ScrollLink>
            </Button>
            <Button className="uppercase mx-3" sx={{ color: "#000", fontWeight: "bold" }}>
              <ScrollLink style={{ color: '#000' }}
                activeClass="active"
                to="contact" // Replace with the ID of the section you want to scroll to
                spy={true}
                smooth={true}
                duration={800}
                offset={-50} // Adjust the offset as needed
              >Contact Us</ScrollLink>
            </Button>
            {/* ))} */}

            <LoginButton
              className="text-white font-bold rounded-none"
              style={{
                width: "12vw",
                background:
                  "transparent linear-gradient(138deg, #001fd6 0%, #19317B 100%) 0% 0% no-repeat padding-box",
              }}
            >
              <ScrollLink
                activeClass="active"
                to="loginBox" // Replace with the ID of the section you want to scroll to
                spy={true}
                smooth={true}
                duration={800}
                offset={-50} // Adjust the offset as needed
              >Login</ScrollLink>
            </LoginButton>
          </Box>
        </Toolbar>
      </AppBar>
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", sm: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </Box>
  );
}
