import * as React from "react";
import { PieChart } from "@mui/x-charts/PieChart";
import { useDrawingArea } from "@mui/x-charts/hooks";
import { styled } from "@mui/material/styles";

const data = [
  { value: 5, label: " A" },
  { value: 10, label: " B" },
  { value: 15, label: " C" },
  { value: 20, label: "D " },
];

const size = {
  width: 400,
  height: 200,
};

const palette = ["#00B3FF", "#0066FF", "#A3EEFF"];

const StyledText = styled("text")(({ theme }) => ({
  fill: "#fff",
  textAnchor: "middle",
  dominantBaseline: "central",
  fontSize: 15,
  color: "#fff",
}));

function PieCenterLabel({ children }: { children: React.ReactNode }) {
  const { width, height, left, top } = useDrawingArea();
  return (
    <StyledText x={left + width / 2} y={top + height / 2}>
      {children}
    </StyledText>
  );
}

export default function PieChartWithCenterLabel() {
  return (
   <div className="w-full">
     <PieChart
      colors={palette}
      series={[
        {
          data,
          highlightScope: { faded: "global", highlighted: "item" },
          innerRadius: 80,
          outerRadius: 140,
          paddingAngle: 5,
          cornerRadius: 5,
          // startAngle: -90,
          // endAngle: 180,
        },
      ]}
      {...size}
      height={300}
    >
      <PieCenterLabel>Center label</PieCenterLabel>
    </PieChart>
   </div>
  );
}
