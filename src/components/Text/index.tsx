import React from "react";

const sizeClasses = {
  txtMontserratBold22WhiteA700: "font-bold font-montserrat",
  txtMontserratBold22: "font-bold font-montserrat",
  txtMontserratMedium16: "font-medium font-montserrat",
  txtMontserratSemiBold18: "font-montserrat font-semibold",
  txtMontserratMedium18: "font-medium font-montserrat",
  txtMontserratRegular14Bluegray900: "font-montserrat font-normal",
  txtMontserratSemiBold35: "font-montserrat font-semibold",
  txtMontserratRegular18: "font-montserrat font-normal",
  txtMontserratMedium18WhiteA700: "font-medium font-montserrat",
  txtMontserratRegular14: "font-montserrat font-normal",
  txtMontserratLight14: "font-light font-montserrat",
  txtMontserratLight14Bluegray900: "font-light font-montserrat",
  txtMontserratMedium18RedA700: "font-medium font-montserrat",
  txtMontserratLight12Black9003f: "font-light font-montserrat",
  txtMontserratBold25: "font-bold font-montserrat",
  txtMontserratMedium14: "font-medium font-montserrat",
  txtMontserratBold18WhiteA700: "font-bold font-montserrat",
  txtMontserratSemiBold14: "font-montserrat font-semibold",
  txtMontserratBold18: "font-bold font-montserrat",
  txtMontserratSemiBold14Black900: "font-montserrat font-semibold",
  txtMontserratMedium14Bluegray900: "font-medium font-montserrat",
  txtMontserratRegular12: "font-montserrat font-normal",
  txtMontserratRegular14IndigoA70002: "font-montserrat font-normal",
  txtMontserratLight12: "font-light font-montserrat",
  txtMontserratMedium14IndigoA70002: "font-medium font-montserrat",
  txtMontserratSemiBold14Bluegray900: "font-montserrat font-semibold",

  txtMontserratRegular14Black900: "font-montserrat font-normal",
  txtMontserratMedium16WhiteA700: "font-medium font-montserrat",
  txtMontserratRomanRegular14: "font-montserrat font-normal",

  txtMontserratRegular14Gray90002: "font-montserrat font-normal",
  txtMontserratLight12WhiteA70087: "font-light font-montserrat",

  txtMontserratMedium14Blue800: "font-medium font-montserrat",
  txtMontserratRegular12Bluegray900: "font-montserrat font-normal",
  txtMontserratMedium16Gray600: "font-medium font-montserrat",
  txtMontserratMedium14WhiteA700: "font-medium font-montserrat",
  txtMontserratMedium14Gray400: "font-medium font-montserrat",
  txtMontserratRegular14WhiteA700: "font-montserrat font-normal",

  txtMontserratRegular22: "font-montserrat font-normal",
  txtMontserratSemiBold14Gray90002: "font-montserrat font-semibold",
  txtMontserratSemiBold16: "font-montserrat font-semibold",
  txtMontserratBold18Gray400: "font-bold font-montserrat",
  txtMontserratRegular18Bluegray900: "font-montserrat font-normal",
  txtMontserratLight12Gray50002: "font-light font-montserrat",
  txtMontserratRomanMedium14: "font-medium font-montserrat",
  txtMontserratRegular16: "font-montserrat font-normal",

  txtMontserratLight14WhiteA700: "font-light font-montserrat",

  txtMontserratRomanMedium14WhiteA700: "font-medium font-montserrat",
  txtMontserratRegular14Gray600: "font-montserrat font-normal",
  txtMontserratMedium14IndigoA700: "font-medium font-montserrat",
  txtMontserratRegular16WhiteA700: "font-montserrat font-normal",
  txtMontserratBold25Bluegray900: "font-bold font-montserrat",

} as const;





export type TextProps = Partial<{
  className: string;
  size: keyof typeof sizeClasses;
  as: any;
}> &
  React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLSpanElement>,
    HTMLSpanElement
  >;

const Text: React.FC<React.PropsWithChildren<TextProps>> = ({
  children,
  className = "",
  size,
  as,
  ...restProps
}) => {
  const Component = as || "p";

  return (
    <Component
      className={`text-left ${className} ${size && sizeClasses[size]}`}
      {...restProps}
    >
      {children}
    </Component>
  );
};

export { Text };
