// VideoPlayer.tsx

import React, { useRef, useState } from 'react';
import { Img } from "components";

interface VideoPlayerProps {
  videoSource: string;
  poster: string;
}

const VideoPlayer: React.FC<VideoPlayerProps> = ({ videoSource, poster }) => {
  const videoRef = useRef<HTMLVideoElement | null>(null);
  const [isPlaying, setIsPlaying] = useState(false);

  const handlePlayPause = () => {
    if (videoRef.current) {
      if (isPlaying) {
        videoRef.current.pause();
      } else {
        videoRef.current.play();
      }
      setIsPlaying(!isPlaying);
    }
  };

  return (
    <div>
      <div className="play-pause-buttons absolute grid justify-center	w-full h-full content-center -mt-[2rem]	inset-x-0 z-50 ">
        {isPlaying ? (
          <div className="pause-button " onClick={handlePlayPause}>
            <Img src="images/login/pause.png" className="h-20 -mt-[1rem]" />
          </div>
        ) : (
          <div className="play-button " onClick={handlePlayPause}>
            <Img src="images/login/play.png" className="h-20 -mt-[8rem]" />
          </div>
        )}
      </div>
      <video ref={videoRef} poster={poster} >
        <source src={videoSource} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    </div>
  );
};

export { VideoPlayer };
