import React from "react";

import { useNavigate } from "react-router-dom";

import { Button, Img, Input, Line, List, Text } from "components";
import { SelectBox } from "components/SelectBox";
import Dashboard from "components/Dashboard";
import { DataTable } from "primereact/datatable";
import DataTableCustom from "components/DataTableCustom";
import PieChartMui from "components/PieChartMui";
import LineChart from "components/LineChart";
import LineChartMui from "components/LineChartMui";

const filterOptionsList = [
  { label: "Option1", value: "option1" },
  { label: "Option2", value: "option2" },
  { label: "Option3", value: "option3" },
];

const DashboardHome = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className="bg-gray-100 flex flex-col font-montserrat items-center justify-end mx-auto pt-3.5 px-3.5 w-full">
        <div className="flex md:flex-col flex-row gap-[29px] items-start justify-between max-w-[1882px] mx-auto md:px-5 w-full">
          <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2 ">
            <Dashboard className=" md:hidden block absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0" />
          </div>

          <div className="flex md:flex-1 flex-col items-start justify-between w-[100%] md:w-full">
            <div className="flex md:flex-col flex-row md:gap-10 items-end justify-between w-full">
              <div className="flex flex-col gap-[25px]  md:mt-0 mt-4">
                <Text
                  className="ml-3 md:ml-[0] text-blue_gray-900 text-sm"
                  size="txtMontserratMedium14Bluegray900"
                >
                  Dashboard
                </Text>
                <Line className="bg-gray-200 h-px w-full" />
              </div>

              <div className="flex gap-1">
                <Input
                  name="groupFortyThree"
                  placeholder="You can search here"
                  className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-full"
                  wrapClassName="flex md:flex-1 md:w-full"
                  suffix={
                    <Img
                      className="h-[37px] ml-[35px] my-auto"
                      src="images/img_rewind.svg"
                      alt="rewind"
                    />
                  }
                  shape="round"
                ></Input>
                <Input
                  name="groupThirtyFive"
                  placeholder="Username lorem"
                  className="!placeholder:text-blue_gray-900 !text-blue_gray-900 font-medium leading-[normal] p-0 text-base text-left w-full"
                  wrapClassName="flex md:flex-1 md:w-full"
                  type="text"
                  suffix={
                    <Img
                      className="h-[37px] ml-3 rounded-[18px] my-auto"
                      src="images/img_rectangle_23.png"
                      alt="Rectangle 23"
                    />
                  }
                  shape="round"
                ></Input>
              </div>
            </div>

            <div className="flex md:flex-col flex-row flex-wrap gap-8 items-center justify-start mt-[25px] w-[100%] md:w-full">
              <div className="shadow-bs1 bg-gradient22 p-3 rounded-3 cursor-pointer flex items-center justify-center min-w-[170px]">
                <div className="!text-white-A700 font-medium leading-[normal] text-left text-sm">
                  ALPR System
                </div>
              </div>
              <Button
                className="common-pointer cursor-pointer flex items-center justify-center min-w-[214px]"
                onClick={() => navigate("/")}
                leftIcon={
                  <div className="mr-1.5 bg-blue_gray-100">
                    <Img
                      src="images/img_settings_blue_gray_100.svg"
                      alt="settings"
                    />
                  </div>
                }
                shape="round"
                color="gray_50_01"
              >
                <div className="font-light leading-[normal] text-left text-sm">
                  Helmet Detection
                </div>
              </Button>
              <Button
                className="cursor-pointer flex items-center justify-center min-w-[197px]"
                leftIcon={
                  <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
                    <Img
                      src="images/img_settings_blue_gray_100_12x34.svg"
                      alt="settings"
                    />
                  </div>
                }
                shape="round"
                color="gray_50_01"
              >
                <div className="font-light leading-[normal] text-left text-sm">
                  Glass Detection
                </div>
              </Button>
            </div>

            <div className="flex gap-5 flex-wrap items-center justify-center w-full">
              <div>
                <div className="flex flex-col items-start justify-start md:w-full gap-2 mt-5">
                  <Text
                    className="ml-0.5 md:ml-[0] text-blue_gray-900 text-lg"
                    size="txtMontserratBold18"
                  >
                    ALPR System
                  </Text>
                  <div className="bg-gradient22  flex flex-col items-start justify-end md:ml-[0] ml-[3px] m-[26px] p-3.5 rounded-[19px] w-full ">
                    <div className="flex flex-col items-center justify-start mt-[13px]">
                      <div className="flex sm:flex-col flex-row sm:gap-5 items-start justify-start ">
                        <Text
                          className="sm:mt-0 mt-1 text-sm text-white-A700"
                          size="txtMontserratSemiBold14"
                        >
                          Today Detection
                        </Text>
                        <Button
                          className="cursor-pointer leading-[normal] min-w-[93px] sm:ml-[0] ml-[201px] rounded-[5px] text-center text-sm"
                          color="white_A700"
                          size="xs"
                        >
                          Cameras
                        </Button>
                        <Text
                          className="sm:ml-[0] ml-[21px] sm:mt-0 mt-[5px] text-sm text-white-A700"
                          size="txtMontserratRegular14WhiteA700"
                        >
                          Zones
                        </Text>
                      </div>
                      <div className="w-full">
                        <PieChartMui/>
                      </div>
                    </div>
                  </div>

                  <div className="bg-white-A700 flex flex-col items-center justify-start mt-[45px] p-[23px] sm:px-5 rounded-[19px] w-full">
                
                    <LineChartMui/>
                  </div>
                </div>
              </div>

              <div className=' rounded-4 card border-0 h-100'>
                <DataTableCustom />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DashboardHome;
