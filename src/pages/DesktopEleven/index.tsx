import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Img, Input, Line, Text } from "components";
import Dashboard from "components/Dashboard";
import RadioButtons from "components/4button";
import { CloseSVG } from "../../assets/images";
import { BASE_URL } from 'apiConfig';
import axios from 'axios';

type DesktopElevenColumnOneProps = Omit<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  "manufacturing" | "language"
> &
  Partial<{ manufacturing: string; language: string; className: string }>;

const DesktopElevenPage: React.FC = () => {
  const [isFormValid, setIsFormValid] = React.useState<boolean>(false);
  const [showValidationMsg, setShowValidationMsg] = React.useState<boolean>(false);
    const [camera_url, setcamera_url] = React.useState<string>('');
  const [name, setcameraname] = React.useState<string>("");
  const [ip_address, setCameraIP] = React.useState<string>("");
  const [location, setCameraLocation] = React.useState<string>("");
  const [cameraData, setCameraData] = useState<{
    name: string;
    camera_url: string;
    location: string;
  }>({
    name: "",
    camera_url: "",
    location: "",
  });

 useEffect(() => {
    const storedCameraName = localStorage.getItem("cameraName");
    const storedCameraLocation = localStorage.getItem("cameraLocation");
    const storedCameraURL = localStorage.getItem("cameraURL");

    if (storedCameraName && storedCameraLocation && storedCameraURL) {
      setCameraData({
        name: storedCameraName,
        camera_url: storedCameraURL,
        location: storedCameraLocation,
      });
    }
  }, []);

  const navigate = useNavigate();

  const addNewCamera = async () => {
  try {
    // Prepare the data payload
    const newCameraData = {
      name: cameraData.name,
      camera_url: cameraData.camera_url,
      location: cameraData.location,
    };

    // Make the POST request to the API endpoint
    const response = await axios.post(`${BASE_URL}/cameras/`, newCameraData);

    // Handle the response, you can navigate or perform other actions based on the API response
    console.log("New camera added:", response.data);
    // Perform any action upon successful addition of the camera

    // Example: Navigate to another page after successfully adding the camera
  
  } catch (error) {
    // Handle error if the API call fails
    console.error("Error adding new camera:", error);
    // Perform any action upon encountering an error, such as displaying an error message
  }
};

    const [groupelevenvalue, setGroupelevenvalue] = React.useState<string>("");
    const [resizedImageData, setResizedImageData] = useState<string | null>(null);

    useEffect(() => {
      // Retrieve the stored JSON data from local storage
      const jsonDataString = localStorage.getItem("resizedImageData");
      if (jsonDataString) {
        const jsonData = JSON.parse(jsonDataString);
        setResizedImageData(jsonData.videoSrc);
      } else {
        // If no resized image data is stored, use the camera URL directly
        setResizedImageData(cameraData.camera_url);
      }
    }, [cameraData.camera_url]);
    
    return (
      <>
        <div className="bg-indigo-50 flex flex-col font-montserrat items-center justify-end mx-auto w-full">
          <div
            className="common-pointer bg-gray-100 flex flex-col items-center justify-start p-3.5 w-full"
            // onClick={() => navigate("/desktopseven")}
          > 
            <div className="flex md:flex-col flex-row gap-4 items-start justify-between max-w-[1883px] mb-[18px] mx-auto md:px-5 w-full">
            <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">

<Dashboard
  className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0"
/>

</div>

              <div className="flex md:flex-1 flex-col gap-[41px] items-center justify-start w-[90%] md:w-full">
                <div className="flex md:flex-col flex-row gap-[50px] items-end justify-between w-full">
                  <div className="flex flex-col gap-[11px] justify-start md:mt-0 mt-[17px]">
                    <Text
                      className="md:ml-[0] ml-[11px] text-base text-blue_gray-900"
                      size="txtMontserratMedium16"
                    >
                      <span className="text-blue_gray-900 font-montserrat text-left font-medium">
                        Add Camera /{" "}
                      </span>
                      <span className="text-blue_gray-900 font-montserrat text-left text-sm font-normal">
                        Camera config
                      </span>
                    </Text>
                    <Line className="white h-px w-full" />
                    </div>
                    <Line className=" h-px w-3/4" />
              
              <Input
                name="groupFiftyOne"
                placeholder="You can search here"
                className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7"
                wrapClassName="flex md:flex-1 md:w-full"
                suffix={
                  <Img
                    className="h-[37px] ml-[35px] my-auto"
                    src="images/img_group_68.svg"
                    alt="Group 68"
                  />
                }
                shape="round"
                color="white_A700"
                size="xs"
                variant="fill"
              ></Input>
              <Button
                  className="bg-white-A700 cursor-pointer flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs"
                  rightIcon={
                    <Img
                      className="h-[37px] ml-3 rounded-[18px]"
                      src="images/img_rectangle_23.png"
                      alt="Rectangle 23"
                    />
                  }
                >
                  <div className="font-medium leading-[normal] sm:pr-5 text-base text-blue_gray-900 text-left">
                    Username
                  </div>
                              </Button>
                </div>
                <div className="bg-white-A700 flex flex-col items-center justify-end p-6 sm:px-5 rounded-[20px] shadow-bs w-full">
                  <div className="flex flex-col justify-start mt-2.5 w-[99%] md:w-full">
                    <Text
                      className="text-[22px] text-blue_gray-900 sm:text-lg md:text-xl"
                      size="txtMontserratBold22"
                    >
                      Add new camera
                    </Text>
                    <Line className="bg-white h-px mr-[3px] mt-[62px] w-full" />

           <text style={{ fontWeight: 'bold', marginBottom: '25 px' }}>
          1.  ADD NEW CAMERA
          </text>
          
                    <div className="flex flex-col items-center justify-start ml-1.5 md:ml-[0] mt-[23px] w-full">
                      <div className="flex md:flex-col flex-row gap-[30px] items-center justify-between w-full">
                        <div className="flex md:flex-1 flex-col items-center justify-start w-[36%] md:w-full">
                        <div className="h-[352px] relative w-full">
  {cameraData.camera_url && (
    <Img
      className="h-[352px] m-auto object-cover rounded-[18px] w-full"
      src={cameraData.camera_url}
      alt="Live Web Image"
    />
  )}

          {/* <Img
            className="absolute h-[352px] inset-[0] justify-center m-auto"
            src="images/img_exclude.svg"
            alt="exclude"
          /> */}
  {/*         
                            <Img
                              className="h-[352px] m-auto object-cover rounded-[18px] w-full"
                              src="images/img_rectangle233.png"
                              alt="rectangle233"
                            />
                            <Img  
                              className="absolute h-[352px] inset-[0] justify-center m-auto"
                              src="images/img_exclude.svg"
                              alt="exclude"
                            /> */}
                          </div>
                        </div>
                        <div className="flex md:flex-1 flex-col items-start justify-start w-[63%] md:w-full">
                        <div className="flex sm:flex-col flex-row sm:gap-[10px] items-center justify-between w-3/5">
                            <Input
 name="groupNinetyEight"
    placeholder="Camera name"
    value={cameraData.name}
    className="leading-[normal] p-0 placeholder:text-blue_gray-900 text-left text-sm w-full"
    wrapClassName="sm:flex-1 rounded-[9px] w-40%]"
    type="text"
    color="gray_100_01"
    size="sm"
    variant="fill"
                            ></Input>
<Input
    name="groupNinetySeven"
    placeholder="Camera URL"
    value={cameraData.camera_url}
    className="leading-[normal] p-0 placeholder:text-blue_gray-900 text-left text-sm w-full"
    wrapClassName="sm:flex-1 rounded-[9px] w-[40%]"
    color="gray_100_01"
    size="sm"
    variant="fill"
></Input>
                          </div>
                          <Input
 name="groupNinetyNine"
  placeholder="Camera Location"
  value={cameraData.location}
  className="leading-[normal] p-0 placeholder:text-blue_gray-900 text-left text-sm w-full"
  wrapClassName="mt-8 rounded-[9px] w-[24%]"
  color="gray_100_01"
  size="sm"
  variant="fill"
                          ></Input>
                          <div className="flex md:flex-col flex-row md:gap-10 items-end justify-between mt-[19px] w-full">
                            <div className="flex flex-col items-center justify-start md:mt-0 mt-[9px]">
                              <Text
                                className="text-blue_gray-900 text-sm w-full"
                                size="txtMontserratLight14Bluegray900"
                              >
                                <>
                                  {/* Lorem Ipsum is simply dummy text of the printing
                                  and typesetting industry. Lorem Ipsum has been
                                  the industry&#39;s{" "} */}
                                </>
                              </Text>
                            </div>
                            <Button
    className="cursor-pointer font-semibold leading-normal mb-1 min-w-[243px] text-base text-center bg-indigo-900 text-white h-12"
    shape="round"
    size="sm"
    variant="fill"
    onClick={() => navigate("/DesktopSeven")}
  >
    Change Camera
    
  </Button>

                          </div>
                          <Line className="bg-white h-px mt-[30px] w-full" />
                          <div className="flex md:flex-col flex-row md:gap-10 items-start justify-between ml-0.5 md:ml-[0] mt-11 w-full">
                            <div className="flex flex-col items-center justify-start md:mt-0 mt-[11px]">
                              <Text
                                className="text-blue_gray-900 text-sm w-full"
                                size="txtMontserratLight14Bluegray900"
                              >
                                <>
                                  {/* Lorem Ipsum is simply dummy text of the printing
                                  and typesetting industry. Lorem Ipsum has been
                                  the industry&#39;s{" "} */}
                                </>
                              </Text>
                            </div>
                            <Button
    className="cursor-pointer font-semibold leading-normal mb-1 min-w-[243px] text-base text-center bg-indigo-900 text-white h-12"
    shape="round"
    size="sm"
    variant="fill"
    onClick={() => navigate("/DesktopSeven")}
  >


                              Change ROI
                            </Button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <Line className="bg-white h-px mr-[3px] mt-[62px] w-full" />

           <text style={{ fontWeight: 'bold', marginBottom: '25 px' }}>
          2.  SELECT INDUSTRY
          </text>
          <Line className="bg-white h-px mr-[3px] mt-[10px] w-full" />
          <text style={{ fontWeight: 'bold ', fontStyle: "oblique" }}>
          Please select the type of industry:
          </text>
          <Line className="bg-white h-px mr-[3px] mt-[10px] w-full" />  
                    <div className="flex flex-col items-center justify-start ml-1 md:ml-[0] mt-[11px]">
                      <Text
                        className="text-blue_gray-900 text-sm"
                        size="txtMontserratLight14Bluegray900"
                      >
                        <>
                          {/* Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry&#39;s standard dummy text ever since the 1500s,
                          when an unknown */}
                        </>
                      </Text>
                    </div>
                  <RadioButtons/>
                    
                    <Button
                      className="common-pointer !rounded-[55px] !text-white-A700 bg-colors2 cursor-not-allowed font-semibold leading-[normal] min-h-[50px] md:ml-[0] ml-[1364px] mt-7 opacity-op text-base text-center w-[220px]"
    onClick={() => {
    addNewCamera();
    navigate("/DesktopSix");
  }}  
                      shape="round"
                      size="sm"
                      variant="gradient"
                      color="indigo_A700_7e_indigo_900_7e"
                    >
                      Add Camera

                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  export default DesktopElevenPage;