import React from "react";
import { useNavigate } from "react-router-dom";
import { Button, Img, Input, Line, Text } from "components";
import Dashboard from "components/Dashboard";
import { CloseSVG } from "../../assets/images";
import DragAndResizeBox from "components/DragAndResizeBox";
import { BASE_URL } from 'apiConfig';

const DesktopSevenPage: React.FC = () => {
  const [showPopup, setShowPopup] = React.useState<boolean>(false);
  const navigate = useNavigate();
  const [camera_url, setcamera_url] = React.useState<string>('');
  const [name, setcameraname] = React.useState<string>("");
  const [ip_address, setCameraIP] = React.useState<string>("");
  const [location, setCameraLocation] = React.useState<string>("");
  const [isFormValid, setIsFormValid] = React.useState<boolean>(false);
  const [showValidationMsg, setShowValidationMsg] = React.useState<boolean>(false);

  React.useEffect(() => {
    setIsFormValid(!!name && !!camera_url && !!location);
  }, [name, camera_url, location]);

  const handleInputChange = (value: string, field: string) => {
    switch (field) {
      case 'groupFive':
        setcameraname(value);
        break;
      case 'camera_url':
        setcamera_url(value);
        break;
      case 'groupThree':
        setCameraLocation(value);
        break;
      default:
        break;
    }
  };

  const openComponentAsPopup = () => {
    setShowPopup(true);
  };

  const saveDataToLocalStorage = () => {
    if (isFormValid) {
      localStorage.setItem('cameraName', name);
      localStorage.setItem('cameraURL', camera_url);
      localStorage.setItem('cameraLocation', location);
    }
  };


  return (
    <>
      <div className="bg-indigo-50 flex flex-col font-montserrat items-center justify-start mx-auto w-full">
        <div className="bg-gray-100 flex flex-col items-center justify-start p-3.5 w-full">
          <div className="flex md:flex-col flex-row gap-4 items-center justify-between max-w-[1883px] mb-[11px] mx-auto md:px-5 w-full">
          <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">

<Dashboard
  className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0"
/>

</div>

            <div className="flex md:flex-1 flex-col gap-[50px] items-center justify-start w-[90%] md:w-full h-[990px]">
              <div className="flex md:flex-col flex-row gap-[50px] items-end justify-between w-full">
  <div className="flex flex-col gap-[0.05px] justify-start md:mt-0 mt-[1px]" style={{ width: '600px' }}>
  <Text
    className="md:ml-[0] ml-[11px] text-base text-blue_gray-900"
    size="txtMontserratMedium16"
  >
    <span className="text-blue_gray-900 font-montserrat text-left font-bold">
      Add Camera /{" "}
    </span>
    <span className="text-blue_gray-900 font-montserrat text-left text-sm font-medium">
      Camera config
    </span>
  </Text>
</div>
                <Line className=" h-px w-3/4" />
              
                <Input
  name="groupFiftyOne"
  placeholder="You can search here"
  className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7"
  wrapClassName="flex md:flex-1 md:w-full"
  suffix={
    <Img
      className="h-[37px] ml-[35px] my-auto"
      src="images/img_group_68.svg"
      alt="Group 68"
    />
  }
  shape="round"
  color="white_A700"
  size="xs"
  variant="fill"
></Input>
              <Button
                  className="bg-white-A700 cursor-pointer flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs"
                  rightIcon={
                    <Img
                      className="h-[37px] ml-3 rounded-[18px]"
                      src="images/img_rectangle_23.png"
                      alt="Rectangle 23"
                    />
                  }
                >
                  <div className="font-medium leading-[normal] sm:pr-5 text-base teaxt-blue_gray-900 text-left">
                    Username
                  </div>
                              </Button>
              </div>
              <div className="bg-white-A700 flex flex-col items-center justify-start p-[100px] sm:px-5 rounded-[20px] shadow-bs" style={{ width: '1600px' }}>
                <div className="flex flex-col items-start justify-start mb-3.5 w-full">
                  <Text
                    className="text-[22px] text-blue_gray-900 sm:text-lg md:text-xl"
                    size="txtMontserratBold22"
                  >
                    ADD NEW CAMERA
                  </Text>
                  <Text
                    className="ml-2 md:ml-[0] mt-[35px] text-blue_gray-900 text-lg font-bold"
                    size="txtMontserratBold22"
                    
                  >
                   1. Add New Camera
                   <Line className="white h-px ml-0.5 md:ml-[0] mt-[20px] w-full font-" />
                  </Text>
                  <div className="flex sm:flex-col flex-row md:gap-20 items-center justify-between mt-[30px]" style={{ width: '1630px' }}>
                    <Input
                      name="groupFive"
                      placeholder="Camera Name"
                      value={name}
                      onChange={(value) => handleInputChange(value, 'groupFive')}
                      className="leading-[normal] p-0 placeholder:bg-gray-100_01 placeholder:text-blue_gray-900 sm:pr-5 text-blue_gray-900 text-left text-sm w-full"
                      wrapClassName="bg-gray-100_01 sm:flex-1 left-[50px] pl-[19px] pr-[150px] py-[17px] relative rounded-[9px] sm:w-full"
                    ></Input>
                    <Input
                     name="Camera Url"
                     placeholder="Camera URL"
                     value={camera_url}
                      onChange={(value) => handleInputChange(value, 'camera_url')}
                      className="leading-[normal] p-0 placeholder:bg-gray-100_01 placeholder:text-blue_gray-900 sm:pr-5 text-blue_gray_900 text-left text-sm w-full"
                      wrapClassName="bg-gray-100_01 sm:flex-1 pl-5 pr-[150px] py-[17px] relative right-20 rounded-[9px] sm:w-full"
                    ></Input>
                    <Input
                      name="groupThree"
                      placeholder="Camera Location"
                      value={location}
                      onChange={(value) => handleInputChange(value, 'groupThree')}
                      className="leading-[normal] p-0 placeholder:bg-gray-100_01 placeholder:text-blue_gray-900 sm:pr-5 text-blue_gray_900 text-left text-sm w-full"
                      wrapClassName="bg-gray-100_01 sm:flex-1 pl-[19px] pr-[150px] py-[17px] relative right-[208px] rounded-[9px] sm:w-full"
                    ></Input>
                  </div>
                  <div className="flex md:flex-col flex-row md:gap-10 items-center justify-between ml-0.5 md:ml-[0] mt-[23px] w-full">
                    <div className="flex flex-col items-center justify-start">
                    {showValidationMsg && (
  <div className="text-red-500 mt-2 text-sm">
    {camera_url === '' ? 'Please fill all the fields.' : 'Please fill all the fields.'}
  </div>
)}
                    </div>
                    {showPopup && (
        <div
          style={{
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            zIndex: 9999,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <div
            style={{
              backgroundColor: 'white',
              padding: '80px', // Adjust the padding as needed
              borderRadius: '10px',
              boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.3)',
              position: 'relative', // Added position relative
            }}
          >
            {/* Close cross icon */}
            <div
              style={{
                position: 'absolute',
                top: '30px',
                right: '30px',
                cursor: 'pointer',
                zIndex: 99999, // Adjust the z-index to appear above other elements
              }}
              onClick={() => setShowPopup(false)}
            >
              <Img
                src="/images/cross.png" // Update the path to your cross image
                alt="Close"
                className="h-[30px]" // Adjust the size as needed
              />
            </div>

            {/* Content inside the popup */}
            <DragAndResizeBox videoSrc={camera_url} />
          </div>
        </div>
      )}
      <Button
        className="common-pointer !bg-colors3 !rounded-[55px] !text-colors4 cursor-pointer font-semibold leading-[normal] min-h-[45px] min-w-[200px] relative right-[50px] text-base text-center"
        shape="round"
        color="blue_gray_900"
        size="sm"
        variant="fill"
        onClick={() => {
          if (!isFormValid || showValidationMsg) {
            setShowValidationMsg(true);
            setTimeout(() => {
              setShowValidationMsg(false);
            }, 5000); // Hides the message after 5 seconds
          } else {
            saveDataToLocalStorage();
            openComponentAsPopup();
          }
        }}
      >
        Select ROI
      </Button>
                  </div>
                  <Line className="white h-px ml-0.5 md:ml-[0] mt-[143px] w-full" />
                  <Button
  className="!rounded-[50px] !text-white-A700 bg-colors5 cursor-pointer font-semibold leading-[normal] min-h-[55px] min-w-[200px] md:ml-[50px] ml-[1200px] mt-1 opacity-op1 text-base text-center"
  shape="round"
  size="sm"
  variant="gradient"
  color="indigo_A700_7e_indigo_900_7e"
  type="button"
  // onClick={handleAddCamera} // Add this line to trigger the API call
>
  Add Camera
</Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DesktopSevenPage;