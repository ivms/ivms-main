import React, { useEffect, useState } from "react";

import { useNavigate } from "react-router-dom";

import { Button, Img, Input, Line, List, Text } from "components";
import Dashboard from "components/Dashboard";
import DesktopSixStack from "components/DesktopSixStack";
import DesktopSixStackOne from "components/DesktopSixStackOne";
import DesktopSixStackTwo from "components/DesktopSixStackTwo";
import DesktopSixStackrectangle188 from "components/DesktopSixStackrectangle188";
import DesktopSixStackrectangle229 from "components/DesktopSixStackrectangle229";
import DesktopSixStackrectangle230 from "components/DesktopSixStackrectangle230";
import './DesktopSix.css'
import { CloseSVG } from "../../assets/images";
import BASE_URL from "apiConfig";

const DesktopSixPage: React.FC = () => {
  const navigate = useNavigate();
  const [connectedCameras, setConnectedCameras] = useState<number>(0);
  const [groupnineteenvalue, setGroupnineteenvalue] =
  React.useState<string>("");
  const [isCameraAdded, setIsCameraAdded] = React.useState<boolean>(false);
  
  // State to store live camera details (assuming you receive it from an API)
  const [liveCameraDetails, setLiveCameraDetails] = React.useState<any>(null);
  const [isLoading, setIsLoading] = useState(true);
  
  // Function to fetch live camera details from the API
  const fetchLiveCameraDetails = async () => {
    try {
      // Make API request to fetch live camera details
      const response = await fetch(`${BASE_URL}/cameras/`);
      const data = await response.json();
      setLiveCameraDetails(data);
      setIsCameraAdded(true);
  
      // Log the data to the console
      console.log("Live Camera Details:", data);
    } catch (error) {
      setIsCameraAdded(false);
    } finally {
      setIsLoading(false);
    }
  };
  
  // useEffect to fetch live camera details when the component mounts
  React.useEffect(() => {
    fetchLiveCameraDetails();
  }, []);
  

  const handleCameraAction = async (Id, action) => {
  try {
    // Send the PUT request to the server to update the camera state
    await fetch(`${BASE_URL}/camera/${Id}/${action}/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      // You may include additional payload if required by your API
    });

    // Update the local state for the specific camera
    setLiveCameraDetails((prevDetails) =>
      prevDetails.map((camera) =>
        camera.id === Id ? { ...camera, isOn: action === 'start' } : camera
      )
    );

    // Reload the page
    window.location.reload();
  } catch (error) {
    console.error(`Error ${action}ing camera:`, error);
  }
};

  
  
 
  useEffect(() => {
    // Fetch data from your API endpoint for connected cameras
    const fetchData = async () => {
      try {
        const response = await fetch(`${BASE_URL}/camera-count/`);
        const data = await response.json();
  
        // Correctly set connectedCameras from the API response
        setConnectedCameras(data.camera_count || 0);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
  
    // Call the fetch data function
    fetchData();
  }, []);
  

  return (
    <>
      <div className="bg-indigo-50 flex flex-col font-montserrat items-center justify-end mx-auto w-full">
        <div className="bg-gray-100 flex flex-col items-center justify-start p-3.5 w-full">
          <div className="flex md:flex-col flex-row gap-4 items-start justify-between max-w-[1883px] mb-4 mx-auto md:px-5 w-full">
          <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">

<Dashboard
  className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0"
/>

</div>

            <div className="flex md:flex-1 flex-col gap-[41px] items-center justify-start w-[90%] md:w-full">
              <div className="flex md:flex-col flex-row gap-[17px] items-end justify-between w-full">
                <div className="flex flex-col gap-[21px] justify-start md:mt-0 mt-[18px]">
<Text
  className="ml-3 md:ml-[0] text-base text-blue_gray-900 whitespace-nowrap"
  size="txtMontserratMedium16"
>
  Camera config
</Text>
                  <Line className="bg-gray-200 h-px w-full" />
                </div>
                <Line className=" h-px w-1/2" />

              <Input
                name="groupFiftyOne"
                placeholder="You can search here"
                className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7"
                wrapClassName="flex md:flex-1 md:w-full"
                suffix={
                  <Img
                    className="h-[37px] ml-[35px] my-auto"
                    src="images/img_group_68.svg"
                    alt="Group 68"
                  />
                }
                shape="round"
                color="white_A700"
                size="xs"
                variant="fill"
              ></Input>
   <Button
  className="bg-white-A700 cursor-pointer flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs"
  rightIcon={
    <Img
      className="h-[37px] ml-3 rounded-[18px]"
      src="images/img_rectangle_23.png"
      alt="Rectangle 23"
    />
  }
>
  <div className="font-medium leading-[normal] sm:pr-5 text-base text-blue_gray-900 text-left">
Username
  </div>
</Button>

              </div>
              <div className="flex flex-col items-start justify-start w-full">
                <div className="bg-white-A700 flex md:flex-col flex-row md:gap-5 items-end justify-start p-[25px] sm:px-5 rounded-[20px] shadow-bs w-full">
                  <Img
                    className="h-[45px] mb-1 ml-2.5 md:ml-[0]"
                    src="images/img_volume.svg"
                    alt="volume"
                  />
                  <div className="flex flex-col gap-1.5 items-start justify-start md:ml-[0] ml-[19px] md:mt-0 mt-1.5">
                    <Text
                      className="text-base text-blue_gray-900"
                      size="txtMontserratMedium16"
                    >
                      Add new camera
                    </Text>
                    <Text
                      className="text-black-900 text-sm"
                      size="txtMontserratLight14"
                    >
                      <>
                        
                      </>
                    </Text>
                  </div>
                  <Button
  className="common-pointer cursor-pointer font-medium leading-[normal] min-w-[243px] md:ml-[0] ml-[1080px] text-base text-center bg-indigo-900 text-white h-12" // Adjusted text color to white and increased the height
  onClick={() => navigate("/desktopseven")}
  shape="round"
  size="sm"
  variant="gradient"
>
  ADD CAMERA
</Button>





                </div>
                <Text
                  className="md:ml-[0] ml-[26px] mt-[38px] text-[22px] text-blue_gray-900 sm:text-lg md:text-xl"
                  size="txtMontserratBold22"
                >
                  Connected Cameras
                  <Line className="bg-white h-px mr-[3px] mt-[20px] w-full" />
                </Text>

                <List
  className="md:flex-1 sm:flex-col flex-row gap-[70px] grid sm:grid-cols-1 md:grid-cols-2 grid-cols-4 md:w-full" style={{ width: '1580px' }}
  orientation="horizontal"
>

                  <div className="bg-gradient4  flex md:flex-1 flex-col items-end justify-start p-[7px] mt-1 rounded-[19px] w-[110%] md:w-full">
                    <div className="flex flex-row gap-7 items-start justify-end mb-[29px] mt-3 w-[105%] md:w-full">
                      <div className="flex flex-col gap-[60px] items-end justify-start w-[60%]">
<Text className="mt-auto text-sm text-white-A700 ml-[42px] w-[80%]"  size="txtMontserratBold18">
{connectedCameras} Cameras Connected
</Text>

                        <div className="h-[193px] relative w-[194px]">
                          <div className="h-[193px] m-auto w-[194px]">
                            <div className="absolute border-[23px] border-indigo-A400 border-solid h-[193px] inset-[0] justify-center m-auto rounded-[96px] w-[193px]"></div>
                            <Img
                              className="absolute h-[188px] inset-[0] justify-center m-auto"
                              src="images/img_ellipse55.svg"
                              alt="ellipseFiftyFive"
                            />
                          </div>
                          <div
                            className="absolute bg-cover bg-no-repeat h-[195px] inset-[0] justify-center m-auto p-[65px] md:px-10 sm:px-5 w-[193px]"
                            style={{
                              backgroundImage: "url('images/img_group31.svg')",
                            }}
                          >
                            <Text
                              className="mb-[-0.79px] mx-auto text-[22px] sm:text-lg text-white-A700 md:text-xl z-[1]"
                              size="txtMontserratBold22WhiteA700"
                            >
                              100%
                            </Text>
                            <Text
                              className="mt-auto mx-auto text-center text-sm text-white-A700"
                              size="txtMontserratRegular14"
                            >
                              <>
                                Cameras
                                <br />
                                Good{" "}
                              </>
                            </Text>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col gap-5 items-center justify-start mt-[86px] w-[33%]">
                        <div className="md:h-[39px] h-[70px] relative w-full">
                        <Text
          className="absolute left-[0] sm:text-[50px] md:text-[50px] text-[25px] text-white-A700 top-[0]"
          size="txtMontserratSemiBold35"
        >
          {connectedCameras}
        </Text>
        <Text
          className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
          size="txtMontserratRegular14"
        >
          cameras connected
        </Text>
                        </div>
                        <div className="md:h-[39px] h-[70px] relative w-full">
                          <Text
                            className="absolute left-[0] sm:text-[31px] md:text-[33px] text-[25px] text-white-A700 top-[0]"
                            size="txtMontserratSemiBold35"
                          >
                            0
                          </Text>
                          <Text
                            className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
                            size="txtMontserratRegular14"
                          >
                            camera disconnected
                          </Text>
                        </div>
                      </div>
                    </div>
                 
                
                </div>
                {isLoading ? (
    <p className="text-blue-500 mt-20 ml-20 text-lg">Loading...</p>
  ) : isCameraAdded && liveCameraDetails.length > 0 ? (
   // Inside the map function
liveCameraDetails.map((camera: any) => (
  <div key={camera.id} style={{ position: 'relative', height: '345px', top: '-12px' }}>
    <div
      className={`bg-gray flex md:flex-1 flex-col items-end justify-start p-[17px] h-[100%] rounded-[19px] w-[115%] h-[100%] md:w-full`}
      key={camera.id}
      style={{ position: 'absolute', top: '-13px', left: '-12px', height: '372px' }}
    >
      <div className="camera-details-container h-[340px] relative w-full">
      <img
  src={camera.camera_url}
  alt={`Live Camera ${camera.id}`}
  className={`your-styles-here ${!camera.is_active ? 'blurred-image' : ''}`} // Apply blurred-image style if is_active is false
/>

        {!camera.is_active && (
          <div className="overlay-off text-white">
            <p>OFF</p>
          </div>
        )}
        <div className="overlay-details text-white">
          <p>Features :</p>
          <p>Name: {camera.name}</p>
          <p>Location: {camera.location}</p>
          {/* Add more details as needed */}
        </div>
      </div>
      {/* Start and Stop buttons */}
      <div className="flex justify-center mt-2">
        {!camera.is_active ? (
          // Render "Start" button if the camera is blurred
          <button
            onClick={() => handleCameraAction(camera.id, 'start')}
            className="bg-green-500 text-white px-4 py-2 rounded-md mr-2"
          >
            Start
          </button>
        ) : (
          // Render "Stop" button if the camera is not blurred
          <button
            onClick={() => handleCameraAction(camera.id, 'stop')}
            className="bg-red-500 text-white px-4 py-2 rounded-md"
          >
            Stop
          </button>
        )}
      </div>
    </div>
  </div>
))

    
  ) : (
    <p className="text-red-500 mt-20 ml-20 text-lg">No Camera Added</p>
  )}
</List>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DesktopSixPage;