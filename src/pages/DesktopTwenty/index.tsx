
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Button, Img, Input, Line, List, Text } from 'components';

import Dashboard from 'components/Dashboard';
import BASE_URL from 'apiConfig';

const DesktopTwentyPage: React.FC = () => {
  const navigate = useNavigate();
  const [tableData, setTableData] = useState([]);
  const [tableHeaders, setTableHeaders] = useState([]);
  const [selectedImage, setSelectedImage] = useState('');
  const [connectedCameras, setConnectedCameras] = useState<number>(0);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10;

  const [userPrivileges, setUserPrivileges] = useState([]);

  const handleViewImage = (imageUrl: string) => {
    setSelectedImage(imageUrl);
  };

  const handleCloseModal = () => {
    setSelectedImage('');
  };

  const handleNextPage = () => {
    setCurrentPage((prevPage) => prevPage + 1);
  };

  const handlePrevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 0));
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${BASE_URL}/dashboard-data/`);
        const data = response.data;

        if (Array.isArray(data) && data.length > 0) {
          setTableHeaders(Object.keys(data[0]));

          const modifiedTableData = data.map((item) => {
            return {
              ...item,
              // Add any modifications needed for each item
            };
          });

          setTableData(modifiedTableData);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`${BASE_URL}/camera-count/`);
        const data = await response.json();
        setConnectedCameras(data.camera_count || 0);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);
  useEffect(() => {
    // Load user privileges from local storage
    const storedPrivileges = localStorage.getItem('userPrivileges');
  
    if (storedPrivileges) {
      const parsedPrivileges = JSON.parse(storedPrivileges);
      console.log('Parsed Privileges:', parsedPrivileges);
      setUserPrivileges(parsedPrivileges);
    }
  }, []);
  
  

 
  return (
   <>

         
      <div className="bg-gray-100 flex flex-col font-montserrat items-center justify-end mx-auto pt-3.5 px-3.5 w-full">
        <div className="flex md:flex-col flex-row gap-4 items-start justify-between max-w-[1882px] mx-auto md:px-5 w-full relative ">
        <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">

<Dashboard
  className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0"
/>

</div>
          <div className="flex md:flex-1 flex-col items-center justify-start w-[90%] md:w-full">
          <div className="flex md:flex-col flex-row md:gap-10 items-end justify-between w-full">
              <div className="flex flex-col gap-[25px] justify-start md:mt-0 mt-4">
                <Text
                  className="ml-3 md:ml-[0] text-blue_gray-900 text-sm"
                  size="txtMontserratMedium14Bluegray900"
                >
                  Dashboard
                </Text>
                <Line className=" h-px w-full" />
              </div>
              <Line className=" h-px w-1/2" />

              <Input
                name="groupFiftyOne"
                placeholder="You can search here"
                className="font-light leading-[normal] relative  p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7"
                wrapClassName="flex md:flex-1 md:w-full"
                suffix={
                  <Img
                    className="h-[37px] ml-[35px] my-auto"
                    src="images/img_group_68.svg"
                    alt="Group 68"
                  />
                }
                shape="round"
                color="white_A700"
                size="xs"
                variant="fill"
              ></Input>
             <Button
                  className="bg-white-A700 cursor-pointer relative  flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs"
                  rightIcon={
                    <Img
                      className="h-[37px] ml-3 rounded-[18px]"
                      src="images/img_rectangle_23.png"
                      alt="Rectangle 23"
                    />
                  }
                >
                  <div className="font-medium leading-[normal] sm:pr-5 text-base text-blue_gray-900 text-left">
                    Username
                  </div>
                </Button>

            </div>
            <div className="flex md:flex-col flex-row gap-8 items-center justify-start mt-[25px] w-[100%] md:w-full">
            {userPrivileges.includes('Alpr detection') && (
   <Button
   className="cursor-pointer flex items-center justify-center min-w-[170px] bg-indigo-900"
   leftIcon={
     <div className="mb-px mr-[9px] w-[21px] ">
       <Img src="images/img_vector.svg" alt="Vector" />
     </div>
   }
   shape="round"
   size="md"
   variant="gradient"
   color="indigo_A700_01_indigo_900"
   onClick={() => navigate("/desktoptwenty")}
 >
   <div className="font-medium leading-[normal] text-left text-sm">
     ALPR System
   </div>
 </Button>
  )}
  {userPrivileges.includes('Human detection') && (
    <Button
      className="common-pointer cursor-pointer flex items-center justify-center min-w-[214px]"
      onClick={() => navigate("/desktoptwentyone")}
      leftIcon={
        <div className="mr-1.5 bg-blue_gray-100">
          <Img src="images/img_vector_blue_gray_100.svg" alt="Vector" />
        </div>
      }
      shape="round"
      color="gray_50"
      size="md"
      variant="fill"
    >
      <div className="font-light leading-[bold] text-left text-sm text-black">
        Human Detection
      </div>
    </Button>
  )}
  {userPrivileges.includes('helmet detection') && (
    <Button
      className="cursor-pointer flex items-center justify-center min-w-[197px]"
      onClick={() => navigate("/desktoptwentythree")}
      leftIcon={
        <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
          <Img src="images/img_union.svg" alt="Union" />
        </div>
      }
      shape="round"
      color="gray_50"
      size="md"
      variant="fill"
    >
      <div className="font-light leading-[normal] text-left text-sm text-black">
        Helmet Detection
      </div>
    </Button>
  )}
 {userPrivileges.includes('Cup detection') && (
    <Button
      className="cursor-pointer flex items-center justify-center min-w-[197px]"
      onClick={() => navigate("/DesktopTwentyTwo")}
      leftIcon={
        <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
          <Img src="images/img_union.svg" alt="Union" />
        </div>
      }
      shape="round"
      color="gray_50"
      size="md"
      variant="fill"
    >
      <div className="font-light leading-[normal] text-left text-sm text-black">
        Cup Detection
      </div>
    </Button>
  )}
</div>


            <div className="flex md:flex-col flex-row md:gap-[49px] items-end justify-between mt-[4px] w-[99%] md:w-full">
              <div className="flex flex-col gap-[26px] justify-start w-[33%] md:w-full">
                <Text
                  className="md:ml-[0] ml-[22px] text-blue_gray-900 text-lg"
                  size="txtMontserratBold18"
                >
                  ALPR System
                </Text>
                <div className="bg-gradient4  flex md:flex-1 flex-col items-end justify-start p-[7px] mt-1 rounded-[19px] w-[100%] md:w-full">
                    <div className="flex flex-row gap-20 items-start justify-end mb-[29px] mt-3 w-[105%] md:w-full ml-[5px]">
                      <div className="flex flex-col gap-[45px] items-end justify-start w-4/5">
                        <Text
                          className="mr-6.0 text-lg text-white-A700 mr-12"
                          size="txtMontserratSemiBold18"
                        >
                         {connectedCameras} Cameras Connected
                        </Text>
                        <div className="h-[193px] relative w-[194px]">
                          <div className="h-[193px] m-auto w-[194px]">
                            <div className="absolute border-[23px] border-indigo-A400 border-solid h-[193px] inset-[0] justify-center m-auto rounded-[96px] w-[193px]"></div>
                            <Img
                              className="absolute h-[188px] inset-[0] justify-center m-auto"
                              src="images/img_ellipse55.svg"
                              alt="ellipseFiftyFive"
                            />
                          </div>
                          <div
                            className="absolute bg-cover bg-no-repeat h-[195px] inset-[0] justify-center m-auto p-[65px] md:px-10 sm:px-5 w-[193px]"
                            style={{
                              backgroundImage: "url('images/img_group31.svg')",
                            }}
                          >
                            <Text
                              className="mb-[-0.79px] mx-auto text-[22px] sm:text-lg text-white-A700 md:text-xl z-[1]"
                              size="txtMontserratBold22WhiteA700"
                            >
                              100%
                            </Text>
                            <Text
                              className="mt-auto mx-auto text-center text-sm text-white-A700"
                              size="txtMontserratRegular14"
                            >
                              <>
                                Cameras
                                <br />
                                Good{" "}
                              </>
                            </Text>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col gap-5 items-center justify-start mt-[86px] w-[33%]">
                        <div className="md:h-[39px] h-[70px] relative w-full">
                        <Text
          className="absolute left-[0] sm:text-[50px] md:text-[50px] text-[25px] text-white-A700 top-[0]"
          size="txtMontserratSemiBold35"
        >
          {connectedCameras} 
        </Text>
        <Text
          className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
          size="txtMontserratRegular14"
        >
          cameras connected
        </Text>
                        </div>
                        <div className="md:h-[39px] h-[70px] relative w-full">
                          <Text
                            className="absolute left-[0] sm:text-[31px] md:text-[33px] text-[25px] text-white-A700 top-[0]"
                            size="txtMontserratSemiBold35"
                          >
                            0
                          </Text>
                          <Text
                            className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
                            size="txtMontserratRegular14"
                          >
                            camera disconnected
                          </Text>
                        </div>
                      </div>
                    </div>
                 
                
                </div>
                <div className="flex flex-col gap-[45px] items-center justify-start w-full">
               
                  <div className="bg-white-A700 flex flex-col items-center justify-start p-[23px] sm:px-5 rounded-[19px] w-full">
                    <div className="flex flex-col items-center justify-start mb-[5px] w-[96%] md:w-full">
                      <div className="flex sm:flex-col flex-row sm:gap-5 items-start justify-start w-full">
                        <Text
                          className="sm:mt-0 mt-[3px] text-black-900 text-sm"
                          size="txtMontserratSemiBold14Black900"
                        >
                          Detection
                        </Text>
                        <Button
                          className="cursor-pointer font-semibold leading-[normal] min-w-[43px] sm:ml-[0] ml-[104px] rounded-[5px] text-center text-sm"
                          color="indigo"
                          size="sm"
                          variant="fill"
                        >
                          1D
                        </Button>
                        <Button className="bg-transparent cursor-pointer leading-[normal] min-w-[25px] ml-9 sm:ml-[0] sm:mt-0 mt-[5px] text-black-900 text-center text-sm">
                          1W
                        </Button>
                        <Button className="bg-transparent cursor-pointer leading-[normal] min-w-[22px] sm:ml-[0] ml-[43px] sm:mt-0 mt-[5px] text-black-900 text-center text-sm">
                          1M
                        </Button>
                        <Button className="bg-transparent cursor-pointer leading-[normal] min-w-[22px] sm:ml-[0] ml-[46px] sm:mt-0 mt-[5px] text-black-900 text-center text-sm">
                          6M
                        </Button>
                        <Button className="bg-transparent cursor-pointer h-[18px] leading-[normal] sm:ml-[0] ml-[46px] sm:mt-0 mt-[5px] text-black-900 text-center text-sm w-[18px]">
                          1Y
                        </Button>
                      </div>
                      <div className="flex sm:flex-col flex-row gap-2 items-center justify-between mt-[42px] w-full">
                        <Text
                          className="text-gray-500_01 text-xs"
                          size="txtMontserratLight12"
                        >
                          1000
                        </Text>
                        <Line className="bg-gray-200_01 h-px sm:mt-0 my-1.5 w-[93%]" />
                      </div>
                      <div className="flex sm:flex-col flex-row gap-3.5 items-center justify-between mt-0.5 w-full">
                        <div className="flex flex-col items-center justify-start">
                          <Text
                            className="text-black-900_3f text-xs"
                            size="txtMontserratLight12Black9003f"
                          >
                            750
                          </Text>
                          <Text
                            className="mt-[33px] text-gray-500_01 text-xs"
                            size="txtMontserratLight12"
                          >
                            500
                          </Text>
                          <Text
                            className="mt-[39px] text-black-900_3f text-xs"
                            size="txtMontserratLight12Black9003f"
                          >
                            250
                          </Text>
                          <Text
                            className="mt-[30px] text-gray-500_01 text-xs"
                            size="txtMontserratLight12"
                          >
                            100
                          </Text>
                        </div>
                        <div className="flex sm:flex-1 flex-col items-center justify-start w-[93%] sm:w-full">
                          <div className="h-[188px] relative w-full">
                            <div className="absolute h-[188px] inset-[0] justify-center m-auto w-full">
                              <div className="absolute flex flex-col h-max inset-[0] items-center justify-center m-auto w-full">
                                <div className="flex flex-col items-center justify-start w-full">
                                  <Line className="bg-gray-200_01 h-px w-full" />
                                  <Line className="bg-gray-200_01 h-px mt-[47px] w-full" />
                                  <Line className="bg-gray-200_01 h-px mt-[53px] w-full" />
                                </div>
                              </div>
                           
                            </div>
                            <Img
                              className="absolute h-[188px] inset-x-[0] mx-auto top-[0]  "
                              src="images/cms.png"
                              alt="vectorSix"
                            
                            />
                          </div>
                          <Line className="bg-gray-200_01 h-px w-full" />
                          <div className="flex flex-row gap-[30px] items-center justify-between mt-4 w-full">
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              12am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              2am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              4am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              6am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              8am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              10am
                            </Text>
                            <Text
                              className="text-center text-gray-500_01 text-xs"
                              size="txtMontserratLight12"
                            >
                              12am
                            </Text>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-white-A700 flex flex-col items-center justify-end md:mt-0 mt-[50px] pt-[21px] rounded-[19px] w-[65%] md:w-full">
                <div className="flex flex-col gap-5 items-center justify-start w-full">
                  <div className="flex md:flex-col flex-row md:gap-5 items-center justify-start w-[106%] md:w-full">
                  <div className="flex flex-row gap-0  justify-start w-48">
                <Text
                      className="text-blue_gray-900 text-sm ml-11 mr-"
                      size="txtMontserratSemiBold14Bluegray900"
                    >
                      ALPR Detection
                    </Text>
                    </div>
                    <List
                      className="sm:flex-col flex-row gap-[18px] grid grid-cols-2 md:ml-[0] ml-[600px] w-1/4 md:w-full"
                      orientation="horizontal"
                    >
                           <Input
                        name="rowfrom"
                        placeholder="From"
                        className="capitalize font-light leading-[normal] p-0 placeholder:text-blue_gray-900 text-blue_gray-900 text-left text-sm w-full"
                        wrapClassName="border border-gray-200 border-solid flex p-[5px] rounded-[5px] w-full"
                        suffix={
                          <Img
                            className="h-[25px]"style={{ width: '35px' }}
                            src="images/img_group126.png"
                            alt="group126"
                          />
                        }
                      ></Input>
                      <Input
                        name="rowto"
                        placeholder="To"
                        className="capitalize font-light leading-[normal] p-0 placeholder:text-blue_gray-900 text-blue_gray-900 text-left text-sm w-full"
                        wrapClassName="border border-gray-200 border-solid flex p-[5px] rounded-[5px] w-full"
                        suffix={
                          <Img
                            className="h-[25px]"style={{ width: '33px' }}
                            src="images/img_group126.png"
                            alt="group125"
                          />
                        }
                      ></Input>
                    </List>

                  </div>
                  <div className="md:h-[4985px] sm:h-[695px] h-[697px] relative w-full">
          {loading && <p style={{ color: 'blue' }}>Loading...</p>}

          {!loading && tableData.length === 0 && (
            <p style={{ color: 'red' }}>No data available.</p>
          )}

          {!loading && tableData.length > 0 && (
            <table className="w-full border-collapse border border-gray-300">
              <thead>
                <tr>
                  <th className="border border-gray-300 p-2">ID</th>
                  <th className="border border-gray-300 p-2">No Plate</th>
                  <th className="border border-gray-300 p-2">Image</th>
                  <th className="border border-gray-300 p-2">Confidence</th>
                  <th className="border border-gray-300 p-2">InTime</th>
                </tr>
              </thead>

              <tbody>
                
                {tableData
                  .slice(
                    currentPage * itemsPerPage,
                    (currentPage + 1) * itemsPerPage
                  )
                  .map((row, rowIndex) => (
                    <tr key={rowIndex}>
                      {tableHeaders.map((header) => (
                        <td key={header} className="border border-gray-300 p-2">
                          {header === 'image' ? (
                            <div className="flex items-center">
                              <Button
                                onClick={() => handleViewImage(row[header])}
                                className="px-2 py-1 bg-blue-500 text-white rounded-md"
                              >
                                View
                              </Button>
                            </div>
                          ) : header === 'intime' ? (
                            <span>
                              {new Date(row[header]).toLocaleString()}{' '}
                              {/* Use toLocaleString or toISOString based on your requirement */}
                            </span>
                          ) : header === 'confidence' ? (
                            <span>{row[header].toFixed(2)}</span>
                          ) : (
                            row[header]
                          )}
                        </td>
                      ))}
                    </tr>
                  ))}
              </tbody>
              
            </table>
          )}

          {selectedImage && (
            <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 flex items-center justify-center">
              <div className="bg-white p-4">
                <div
                  style={{ width: '400px', height: '300px', position: 'relative' }}
                >
                  <Img
                    src={selectedImage}
                    alt="Image"
                    className="w-full h-full object-cover"
                  />
                </div>
                <Button onClick={handleCloseModal}>Close</Button>
              </div>
              
            </div>
          )}
         {/* Pagination controls */}
<div className="flex justify-end mt-4">
  {(!loading && tableData.length > 0) && (
    <div className="mr-4">
      <span className="mr-2">
        Page {currentPage + 1}/{Math.ceil(tableData.length / itemsPerPage)}
      </span>
      <button
        onClick={handlePrevPage}
        disabled={currentPage === 0}
        style={{
          padding: '10px',
          backgroundColor: currentPage === 0 ? '#bdc3c7' : '#3498db',
          color: '#fff',
          border: 'none',
          borderRadius: '5px',
          cursor: currentPage === 0 ? 'not-allowed' : 'pointer',
          outline: 'none',
        }}
      >
        Prev
      </button>
      <button
        onClick={handleNextPage}
        disabled={currentPage === Math.ceil(tableData.length / itemsPerPage) - 1}
        style={{
          marginLeft: '8px',
          padding: '10px',
          backgroundColor: currentPage === Math.ceil(tableData.length / itemsPerPage) - 1 ? '#bdc3c7' : '#3498db',
          color: '#fff',
          border: 'none',
          borderRadius: '5px',
          cursor: currentPage === Math.ceil(tableData.length / itemsPerPage) - 1 ? 'not-allowed' : 'pointer',
          outline: 'none',
        }}
      >
        Next
      </button>
    </div>
  )}
</div>

       
        </div>

     
      </div>
                </div>
                
              </div>
              
            </div>
          </div>
        </div>
       
    </>
  );
};

export default DesktopTwentyPage;