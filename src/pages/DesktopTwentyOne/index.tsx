import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Button, Img, Input, Line, List, Text } from 'components';

import Dashboard from 'components/Dashboard';
import BASE_URL from 'apiConfig';
import { Line as ChartLine } from 'react-chartjs-2';
import LineChart from 'components/LineChart';




const filterOptionsList = [
  { label: 'Option1', value: 'option1' },
  { label: 'Option2', value: 'option2' },
  { label: 'Option3', value: 'option3' },
];

const DesktopTwentyonePage: React.FC = () => {
  const [chartData, setChartData] = useState<any>({
    labels: ["12am", "2am", "4am", "6am", "8am", "10am", "12pm"],
    datasets: [
      {
        label: "My Dataset",
        data: [0, 250, 500, 750, 1000, 750, 500],
        fill: false,
        borderColor: "rgba(75,192,192,1)",
      },
    ],
  });
  const navigate = useNavigate();
  const [tableData, setTableData] = useState([]);
  const [tableHeaders, setTableHeaders] = useState([]);
  const [selectedImage, setSelectedImage] = useState('');
  const [connectedCameras, setConnectedCameras] = useState<number>(0);
  const [userPrivileges, setUserPrivileges] = useState([]);

  const handleViewImage = (imageUrl: string) => {
    setSelectedImage(imageUrl);
  };

  const handleCloseModal = () => {
    setSelectedImage('');
  };

  

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${BASE_URL}/dashboard-data/`);
        const data = response.data;
        console.log('API Response:', data);

        if (Array.isArray(data) && data.length > 0) {
          setTableHeaders(Object.keys(data[0]));

          const modifiedTableData = data.map(item => {
            return {
              ...item,
              // Add any modifications needed for each item
            };
          });

          setTableData(modifiedTableData);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    

    fetchData();
  }, []); 

  useEffect(() => {
    // Load user privileges from local storage
    const storedPrivileges = localStorage.getItem('userPrivileges');
  
    if (storedPrivileges) {
      const parsedPrivileges = JSON.parse(storedPrivileges);
      console.log('Parsed Privileges:', parsedPrivileges);
      setUserPrivileges(parsedPrivileges);
    }
  }, []);


  return (
   <>
         <div className="bg-gray-100 flex flex-col font-montserrat items-center justify-end mx-auto pt-3.5 px-3.5 w-full">
        <div className="flex md:flex-col flex-row gap-4 items-start justify-between max-w-[1882px] mx-auto md:px-5 w-full relative ">
       
        <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">

<Dashboard
  className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0"
/>
  
</div>

          <div className="flex md:flex-1 flex-col items-center justify-start w-[90%] md:w-full">
         <div className="relative flex md:flex-col flex-row md:gap-10 items-end justify-between w-full">
  <div className="flex flex-col gap-[25px] justify-start md:mt-0 mt-4 relative">
    <Text
      className="ml-3 md:ml-[0] text-blue_gray-900 text-sm"
      size="txtMontserratMedium14Bluegray900"
    >
      Dashboard
    </Text>
    <Line className="h-px w-full absolute bottom-0" />
  </div>
  <Line className="h-px w-1/2 absolute bottom-0" />

  <Input
    name="groupFiftyOne"
    placeholder="You can search here"
    className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7 relative mr-2"
    wrapClassName="flex md:flex-1 md:w-full"
    suffix={
      <Img
        className="h-[37px] ml-[35px] my-auto"
        src="images/img_group_68.svg"
        alt="Group 68"
      />
    }
    shape="round"
    color="white_A700"
    size="xs"
    variant="fill"
  />
  
  <Button
    className="bg-white-A700 cursor-pointer flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs relative"
    rightIcon={
      <Img
        className="h-[37px] ml-3 rounded-[18px]"
        src="images/img_rectangle_23.png"
        alt="Rectangle 23"
      />
    }
  >
    <div className="font-medium leading-[normal] sm:pr-5 text-base text-blue_gray-900 text-left">
      Username lorem
    </div>
  </Button>
</div>

            <div className="flex md:flex-col flex-row gap-8 items-center justify-start mt-[25px] w-[100%] md:w-full">
           

            {userPrivileges.includes('Alpr detection') && (
    <Button
      className="cursor-pointer flex items-center justify-center min-w-[170px] bg-indigo-900"
      leftIcon={
        <div className="mb-px mr-[9px] w-[21px] ">
          <Img src="images/img_vector.svg" alt="Vector" />
        </div>
      }
      shape="round"
      size="md"
      variant="gradient"
      color="indigo_A700_01_indigo_900"
      onClick={() => navigate("/desktoptwenty")}
    >
      <div className="font-medium leading-[normal] text-left text-sm">
        ALPR System
      </div>
    </Button>
  )}
  {userPrivileges.includes('Human detection') && (
    <Button
    className="cursor-pointer flex items-center justify-center min-w-[170px] bg-indigo-900"
    onClick={() => navigate("/desktoptwentyone")}

      leftIcon={
      <div className="mr-1.5 bg-blue_gray-100">
        <Img
          src="images/img_vector_blue_gray_100.svg"
          alt="Vector"
        />
      </div>
    }
    shape="round"
    size="md"
    variant="gradient"
    color="indigo_A700_01_indigo_900"
  >
    <div className="font-medium leading-[normal] text-left text-sm">
        Human Detection
      </div>
    </Button>
  )}
  {userPrivileges.includes('helmet detection') && (
    <Button
      className="cursor-pointer flex items-center justify-center min-w-[197px]"
      onClick={() => navigate("/desktoptwentythree")}
      leftIcon={
        <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
          <Img src="images/img_union.svg" alt="Union" />
        </div>
      }
      shape="round"
      color="gray_50"
      size="md"
      variant="fill"
    >
      <div className="font-light leading-[normal] text-left text-sm text-black">
        Helmet Detection
      </div>
    </Button>
  )}
 {userPrivileges.includes('Cup detection') && (
    <Button
      className="cursor-pointer flex items-center justify-center min-w-[197px]"
      onClick={() => navigate("/DesktopTwentyTwo")}
      // leftIcon={
      //   // // <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
      //   //   <Img src="" alt="Union" />
      //   // </div>
      // }
      shape="round"
      color="gray_50"
      size="md"
      variant="fill"
    >
      <div className="font-light leading-[normal] text-left text-sm text-black">
        Cup Detection
      </div>
    </Button>
  )}
            </div>
            <div className="flex md:flex-col flex-row md:gap-[49px] items-end justify-between mt-[43px] w-[99%] md:w-full">
              <div className="flex flex-col gap-[50px] justify-start w-[33%] md:w-full">
                <Text
                  className="md:ml-[0] ml-[22px] text-blue_gray-900 text-lg"
                  size="txtMontserratBold18"
                >
                 Human Detection
                </Text>
                <div className="bg-gradient4  flex md:flex-1 flex-col items-end justify-start p-[7px] mt-1 rounded-[19px] w-[100%] md:w-full">
                    <div className="flex flex-row gap-20 items-start justify-end mb-[29px] mt-3 w-[105%] md:w-full ml-[5px]">
                      <div className="flex flex-col gap-[45px] items-end justify-start w-4/5">
                        <Text
                          className="mr-6.0 text-sm text-white-A700 mr-12"
                          size="txtMontserratSemiBold18"
                        >
                         {connectedCameras} Cameras Connected
                        </Text>
                        <div className="h-[193px] relative w-[194px]">
                          <div className="h-[193px] m-auto w-[194px]">
                            <div className="absolute border-[23px] border-indigo-A400 border-solid h-[193px] inset-[0] justify-center m-auto rounded-[96px] w-[193px]"></div>
                            <Img
                              className="absolute h-[188px] inset-[0] justify-center m-auto"
                              src="images/img_ellipse55.svg"
                              alt="ellipseFiftyFive"
                            />
                          </div>
                          <div
                            className="absolute bg-cover bg-no-repeat h-[193px] inset-[0] justify-center m-auto p-[65px] md:px-10 sm:px-5 w-[193px]"
                            style={{
                              backgroundImage: "url('images/img_group31.svg')",
                            }}
                          >
                            <Text
                              className="mb-[-0.79px] mx-auto text-[22px] sm:text-lg text-white-A700 md:text-xl z-[1]"
                              size="txtMontserratBold22WhiteA700"
                            >
                              100%
                            </Text>
                            <Text
                              className="mt-auto mx-auto text-center text-sm text-white-A700"
                              size="txtMontserratRegular14"
                            >
                              <>
                                Cameras
                                <br />
                                Good{" "}
                              </>
                            </Text>
                          </div>
                        </div>
                      </div>
                      <div className="flex flex-col gap-5 items-center justify-start mt-[86px] w-[33%]">
                        <div className="md:h-[39px] h-[70px] relative w-full">
                        <Text
          className="absolute left-[0] sm:text-[50px] md:text-[50px] text-[25px] text-white-A700 top-[0]"
          size="txtMontserratSemiBold35"
        >
          {connectedCameras}
        </Text>
        <Text
          className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
          size="txtMontserratRegular14"
        >
          cameras connected
        </Text>
                        </div>
                        <div className="md:h-[39px] h-[70px] relative w-full">
                          <Text
                            className="absolute left-[0] sm:text-[31px] md:text-[33px] text-[25px] text-white-A700 top-[0]"
                            size="txtMontserratSemiBold35"
                          >
                            0
                          </Text>
                          <Text
                            className="absolute bottom-[0] inset-x-[0] mx-auto text-sm text-white-A700 w-full"
                            size="txtMontserratRegular14"
                          >
                            camera disconnected
                          </Text>
                        </div>
                      </div>
                    </div>
                 
                
                </div>
                <div className="flex flex-col gap-[45px] items-center justify-start w-full">
                <LineChart data={chartData} />
                 
                </div>
              </div>
              <div className="bg-white-A700 flex flex-col items-center justify-end md:mt-0 mt-[50px] pt-[21px] rounded-[19px] w-[65%] md:w-full">
                <div className="flex flex-col gap-5 items-center justify-start w-full">
                 
                  <div className="md:h-[4985px] sm:h-[695px] h-[697px] relative w-full">
      
        <table className="w-full border-collapse border border-gray-300">
          <thead>
  <tr>
    <th className="border border-gray-300 p-2">ID</th>
    <th className="border border-gray-300 p-2">Anomaly</th>
  
    
    <th className="border border-gray-300 p-2">Date</th>
  </tr>
</thead>
        
          <tbody>
      
      </tbody>

        </table>
      
      {selectedImage && (
        <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 flex items-center justify-center">
          <div className="bg-white p-4">
            {/* Fixed size for the image container */}
            <div style={{ width: '400px', height: '300px', position: 'relative' }}>
              <Img src={selectedImage} alt="Image" className="w-full h-full object-cover" />
            </div>
            <Button onClick={handleCloseModal}>Close</Button>
          </div>
        </div>
      )}
      
    </div>
    
    
                </div>
                
              </div>
              
            </div>
          </div>
        </div>
        </div>
    </>
  );
};

export default DesktopTwentyonePage;