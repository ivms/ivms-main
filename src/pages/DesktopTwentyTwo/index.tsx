import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { Button, Img, Input, Line, List, Text } from "components";

import Dashboard from "components/Dashboard";
import BASE_URL from "apiConfig";

const DesktopTwentyTwoPage: React.FC = () => {
  const navigate = useNavigate();
  const [tableData, setTableData] = useState([]);
  const [tableHeaders, setTableHeaders] = useState([]);
  const [selectedImage, setSelectedImage] = useState("");
  const [connectedCameras, setConnectedCameras] = useState<number>(0);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10;
  const [userPrivileges, setUserPrivileges] = useState([]);
  const [messages, setMessages] = useState([]);

  const [cupCount, setCupCount] = useState("");
  const [receivedFrame, setReceivedFrame] = useState(null);
  const socketRef = useRef(null);
  const [hasReceivedResponse, setHasReceivedResponse] = useState(false);
  const [isWebSocketActive, setIsWebSocketActive] = useState(false);

  const handleStartButtonClick = () => {
    if (!isWebSocketActive) {
      // Start the WebSocket connection only if it's not already active
      socketRef.current = new WebSocket(
        "ws://65.108.149.173:8008/ws/cup-count/"
      );
      socketRef.current.onopen = function () {
        console.log("Socket connected!");
        sendStartMessage();
        setIsWebSocketActive(true); // Set the flag to indicate WebSocket is active
      };

      socketRef.current.onmessage = function (event) {
        const receivedMessage = JSON.parse(event.data);
        // Process received messages
        if (
          receivedMessage.frame &&
          receivedMessage.cup_count &&
          !hasReceivedResponse
        ) {
          const { frame, cup_count } = receivedMessage;
          setCupCount(`Cup Count: ${cup_count}`);
          setReceivedFrame(convertBase64ToImage(frame));
          setHasReceivedResponse(true);
        }
      };
    }
  };

  const handleStopButtonClick = () => {
    if (isWebSocketActive) {
      // Close the WebSocket connection only if it's active
      socketRef.current.close();
      setIsWebSocketActive(false); // Set the flag to indicate WebSocket is inactive
      setHasReceivedResponse(false); // Reset the response flag
      setCupCount(""); // Reset cup count
      setReceivedFrame(null); // Reset received frame
    }
  };

  const sendStartMessage = () => {
    const startMessage = {
      type: "start",
    };

    if (socketRef.current.readyState === WebSocket.OPEN) {
      socketRef.current.send(JSON.stringify(startMessage));
    } else {
      // WebSocket connection not yet open, wait for onopen event
      socketRef.current.onopen = function () {
        socketRef.current.send(JSON.stringify(startMessage));
        setIsWebSocketActive(true); // Set the flag to indicate WebSocket is active
      };
    }
  };

  function convertBase64ToImage(base64Str) {
    return (
      <img src={`data:image/jpeg;base64, ${base64Str}`} alt="Received Frame" />
    );
  }

  const handleCloseModal = () => {
    setSelectedImage("");
  };

  useEffect(() => {
    // Load user privileges from local storage
    const storedPrivileges = localStorage.getItem("userPrivileges");

    if (storedPrivileges) {
      const parsedPrivileges = JSON.parse(storedPrivileges);
      console.log("Parsed Privileges:", parsedPrivileges);
      setUserPrivileges(parsedPrivileges);
    }
  }, []);

  useEffect(() => {
    // Fetch data from the API
    fetch("https://cv.beaconinfotech.online/facility/cup-count/")
      .then((response) => response.json())
      .then((data) => {
        setTableData(data); // Set the fetched data to the state
      })
      .catch((error) => console.error("Error fetching data:", error));
  }, []);
  return (
    <>
      <div className="bg-gray-100 flex flex-col font-montserrat items-center justify-end mx-auto pt-3.5 px-3.5 w-full">
        <div className="flex md:flex-col flex-row gap-4 items-start justify-between max-w-[1882px] mx-auto md:px-5 w-full relative ">
          <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2">
            <Dashboard className="absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0" />
          </div>
          <div className="flex md:flex-1 flex-col items-center justify-start w-[90%] md:w-full">
            <div className="flex md:flex-col flex-row md:gap-10 items-end justify-between w-full">
              <div className="flex flex-col gap-[25px] justify-start md:mt-0 mt-4">
                <Text
                  className="ml-3 md:ml-[0] text-blue_gray-900 text-sm"
                  size="txtMontserratMedium14Bluegray900"
                >
                  Dashboard
                </Text>
                <Line className=" h-px w-full" />
              </div>
              <Line className=" h-px w-1/2" />

              <Input
                name="groupFiftyOne"
                placeholder="You can search here"
                className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-64 h-7"
                wrapClassName="flex md:flex-1 md:w-full"
                suffix={
                  <Img
                    className="h-[37px] ml-[35px] my-auto"
                    src="images/img_group_68.svg"
                    alt="Group 68"
                  />
                }
                shape="round"
                color="white_A700"
                size="xs"
                variant="fill"
              ></Input>
              <Button
                className="bg-white-A700 cursor-pointer flex items-center justify-center min-w-[277px] pl-5 pr-[35px] py-3 rounded-[20px] shadow-bs"
                rightIcon={
                  <Img
                    className="h-[37px] ml-3 rounded-[18px]"
                    src="images/img_rectangle_23.png"
                    alt="Rectangle 23"
                  />
                }
              >
                <div className="font-medium leading-[normal] sm:pr-5 text-base text-blue_gray-900 text-left">
                  Username
                </div>
              </Button>
            </div>
            <div className="flex md:flex-col flex-row gap-8 items-center justify-start mt-[25px] w-[100%] md:w-full">
              {userPrivileges.includes("Alpr detection") && (
                <Button
                  className="cursor-pointer flex items-center justify-center min-w-[170px] bg-indigo-900"
                  leftIcon={
                    <div className="mb-px mr-[9px] w-[21px] ">
                      <Img src="images/img_vector.svg" alt="Vector" />
                    </div>
                  }
                  shape="round"
                  size="md"
                  variant="gradient"
                  color="indigo_A700_01_indigo_900"
                  onClick={() => navigate("/desktoptwenty")}
                >
                  <div className="font-medium leading-[normal] text-left text-sm">
                    ALPR System
                  </div>
                </Button>
              )}
              {userPrivileges.includes("Human detection") && (
                <Button
                  className="common-pointer cursor-pointer flex items-center justify-center min-w-[214px]"
                  onClick={() => navigate("/desktoptwentyone")}
                  leftIcon={
                    <div className="mr-1.5 bg-blue_gray-100">
                      <Img
                        src="images/img_vector_blue_gray_100.svg"
                        alt="Vector"
                      />
                    </div>
                  }
                  shape="round"
                  color="gray_50"
                  size="md"
                  variant="fill"
                >
                  <div className="font-light leading-[bold] text-left text-sm text-black">
                    Human Detection
                  </div>
                </Button>
              )}
              {userPrivileges.includes("helmet detection") && (
                <Button
                  className="cursor-pointer flex items-center justify-center min-w-[197px]"
                  onClick={() => navigate("/desktoptwentythree")}
                  leftIcon={
                    <div className="mt-[3px] mb-0.5 mr-[7px] bg-blue_gray-100">
                      <Img src="images/img_union.svg" alt="Union" />
                    </div>
                  }
                  shape="round"
                  color="gray_50"
                  size="md"
                  variant="fill"
                >
                  <div className="font-light leading-[normal] text-left text-sm text-black">
                    Helmet Detection
                  </div>
                </Button>
              )}
              {userPrivileges.includes("Cup detection") && (
                <Button
                  className="cursor-pointer flex items-center justify-center min-w-[170px] bg-indigo-900"
                  // leftIcon={
                  //   <div className="mb-px mr-[9px] w-[21px] ">
                  //     <Img src="images/img_vector.svg" alt="Vector" />
                  //   </div>
                  // }
                  shape="round"
                  size="md"
                  variant="gradient"
                  color="indigo_A700_01_indigo_900"
                >
                  <div className="font-medium leading-[normal] text-left text-sm">
                    Cup Detection
                  </div>
                </Button>
              )}
            </div>
            <div className="flex md:flex-col flex-row md:gap-[49px] items-end justify-between mt-[4px] w-[99%] md:w-full">
              <div className="flex flex-col gap-[26px] justify-start w-[33%] md:w-full">
                <Text
                  className="md:ml-[0] ml-[20px] text-blue_gray-900 text-lg mt-10 mb-0.5"
                  size="txtMontserratBold18"
                >
                  Cup System
                </Text>
                {!isWebSocketActive ? (
                  <button
                    onClick={handleStartButtonClick}
                    className="btn-start bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded w-24"
                  >
                    Start
                  </button>
                ) : (
                  <button
                    onClick={handleStopButtonClick}
                    className="btn-stop bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded w-24"
                  >
                    Stop
                  </button>
                )}

                <div className="flex flex-col gap-[45px] items-center justify-start w-3/4 ml-10">
                  <div
                    className="rounded-lg overflow-hidden"
                    style={{ width: "470px", height: "425px" }}
                  >
                    {receivedFrame}
                  </div>
                </div>
                <div className="bg-gray-0 p-4 rounded-[19px] w-[90%] md:w-full">
                  <div className="flex md:flex-1 flex-col items-end justify-start rounded-[19px] w-[100%] md:w-full">
                    <div className="flex flex-row justify-center w-full mb-[29px] mt-3">
                      <div className="flex-1 mx-3">
                        <div className="rounded-lg bg-gradient4 p-4 h-48 w-52 mb-2 text-white">
                          <p>Total Cups: {cupCount}</p>
                        </div>
                      </div>
                      <div className="flex-1 mx-3">
                        <div className="rounded-lg bg-gradient4 p-4 h-48 w-52 text-white">
                          <p>Total Large Cups: 0</p>
                        </div>
                      </div>
                    </div>
                    <div className="flex flex-row justify-center w-full">
                      <div className="flex-1 mx-3">
                        <div className="rounded-lg bg-gradient4 p-4 h-48 w-52 mb-3 text-white">
                          <p>Total Medium Cups: 0</p>
                        </div>
                      </div>
                      <div className="flex-1 mx-3">
                        <div className="rounded-lg bg-gradient4 p-4 h-48 w-52 text-white">
                          <p>Total Small Cups: 0</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-white-A700 flex flex-col items-center justify-end md:mt-0 mt-[1px] pt-[6px] pb-[16px] rounded-[19px] w-[70%] md:w-full h-[990px]">
                <div className="flex flex-col gap-8 items-center justify-start w-full mb-10 h-[920px]">
                  <div className="flex md:flex-col flex-row md:gap-5 items-center justify-start w-[106%] md:w-full">
                    <div className="flex flex-row gap-0  justify-start w-52">
                      <Text
                        className="text-blue_gray-900 text-sm ml-11 mr-"
                        size="txtMontserratSemiBold14Bluegray900"
                      >
                        Cup Detection
                      </Text>
                    </div>
                    <List
                      className="sm:flex-col flex-row gap-[18px] grid grid-cols-2 md:ml-[0] ml-[600px] w-1/4 md:w-full"
                      orientation="horizontal"
                    >
                      <Input
                        name="rowfrom"
                        placeholder="From"
                        className="capitalize font-light leading-[normal] p-0 placeholder:text-blue_gray-900 text-blue_gray-900 text-left text-sm w-full"
                        wrapClassName="border border-gray-200 border-solid flex p-[5px] rounded-[5px] w-full mr-8"
                        suffix={
                          <Img
                            className="h-[25px]"
                            style={{ width: "35px" }}
                            src="images/img_group126.png"
                            alt="group126"
                          />
                        }
                      ></Input>
                      <Input
                        name="rowto"
                        placeholder="To"
                        className="capitalize font-light leading-[normal] p-0 placeholder:text-blue_gray-900 text-blue_gray-900 text-left text-sm w-full"
                        wrapClassName="border border-gray-200 border-solid flex p-[5px] rounded-[5px] w-full mr-4"
                        suffix={
                          <Img
                            className="h-[25px]"
                            style={{ width: "33px" }}
                            src="images/img_group126.png"
                            alt="group125"
                          />
                        }
                      ></Input>
                    </List>
                  </div>
                  <div className="md:h-[4990px] sm:h-[695px] h-[700px] relative w-full">
                    <table className="w-full border-collapse border border-gray-300 mb-">
                      <thead>
                        <tr>
                          <th className="border border-gray-300 p-2">Sl No</th>
                          <th className="border border-gray-300 p-2">
                            Total Cup
                          </th>
                          <th className="border border-gray-300 p-2">Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        {tableData.map((item, index) => (
                          <tr key={index}>
                            <td className="border border-gray-300 p-2">
                              {index + 1}
                            </td>
                            <td className="border border-gray-300 p-2">
                              {item.cup_count}
                            </td>
                            <td className="border border-gray-300 p-2">
                              {item.date}
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>

                    {selectedImage && (
                      <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 flex items-center justify-center">
                        <div className="bg-white p-4">
                          {/* Fixed size for the image container */}
                          <div
                            style={{
                              width: "400px",
                              height: "300px",
                              position: "relative",
                            }}
                          >
                            <Img
                              src={selectedImage}
                              alt="Image"
                              className="w-full h-full object-cover"
                            />
                          </div>
                          <Button onClick={handleCloseModal}>Close</Button>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DesktopTwentyTwoPage;
