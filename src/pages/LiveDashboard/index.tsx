import React from "react";

import { useNavigate } from "react-router-dom";

import { Button, Img, Input, Line, List, Text } from "components";
import { SelectBox } from "components/SelectBox";
import Dashboard from "components/Dashboard";
import { DataTable } from "primereact/datatable";
import DataTableCustom from "components/DataTableCustom";
import PieChartMui from "components/PieChartMui";
import LineChart from "components/LineChart";
import LineChartMui from "components/LineChartMui";
import "./livedashboard.css";
import { useState } from "react";
import { url } from "inspector";

const filterOptionsList = [
  { label: "Option1", value: "option1" },
  { label: "Option2", value: "option2" },
  { label: "Option3", value: "option3" },
];

const LoopDiv = ({ imageUrl }) => {
  const [toggle, setToggle] = useState(false);
  const toggleStart = () => {
    setToggle(!toggle);
  };
  console.log(toggle);

 

  return (
    <>
      <div className="card cardImage">
        <img src={imageUrl} alt="" />
        <div className="content1 font-thin w-full">
          <div className="flex w-[100%] justify-between items-center">
            <div className="w-full">
              <p className="text-md font-thin"> Use Case name</p>
              <p className="text-sm font-thin"> 1096 Detection</p>
            </div>
            <div onClick={toggleStart}>
              <button className={`button ${toggle ? "paused" : ""}`}></button>
            </div>
          </div>
          <div className="extraDetails items-center">
            <div className="extraData">
              <p> 30% more </p>
              <span> than Yesterday </span>
            </div>
            <div>
              <button className="bg-gradient22 p-2 w-[100px] rounded-3">
                View
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const LiveDashboard = () => {
  const navigate = useNavigate();
  const imageUrls = [
    "https://webcam.schwaebischhall.de/mjpg/video.mjpg",
    "https://webcam.privcom.ch/mjpg/video.mjpg",

  ];

  return (
    <div>
      <div className="flex w-full m-4">
        <div className="md:h-[1000px] h-[990px] relative w-[10%] md:w-1/2 ">
          <Dashboard className=" md:hidden block absolute bg-white-A700 flex flex-col gap-[32px] h-90 inset-y-[0] items-center justify-center left-[0] my-auto p-7 sm:px-5 rounded-[20px] shadow-bs max-w-[60%] w-auto md:w-full mt-0" />
        </div>
        <div className=" w-[87%] ">
          <div className="flex md:flex-col flex-row md:gap-10 items-start justify-between w-full m-4">
            <div className="flex flex-col gap-[25px]  md:mt-0 mt-4">
              <Text
                className="ml-3 md:ml-[0] text-blue_gray-900 text-sm"
                size="txtMontserratMedium14Bluegray900"
              >
                Dashboard
              </Text>
              <Line className="bg-gray-200 h-px w-full" />
            </div>

            <div className="flex gap-1">
              <Input
                name="groupFortyThree"
                placeholder="You can search here"
                className="font-light leading-[normal] p-0 placeholder:text-blue_gray-400 text-left text-sm w-full"
                wrapClassName="flex md:flex-1 md:w-full"
                suffix={
                  <Img
                    className="h-[37px] ml-[35px] my-auto"
                    src="images/img_rewind.svg"
                    alt="rewind"
                  />
                }
                shape="round"
              ></Input>
              <Input
                name="groupThirtyFive"
                placeholder="Username lorem"
                className="!placeholder:text-blue_gray-900 !text-blue_gray-900 font-medium leading-[normal] p-0 text-base text-left w-full"
                wrapClassName="flex md:flex-1 md:w-full"
                type="text"
                suffix={
                  <Img
                    className="h-[37px] ml-3 rounded-[18px] my-auto"
                    src="images/img_rectangle_23.png"
                    alt="Rectangle 23"
                  />
                }
                shape="round"
              ></Input>
            </div>
          </div>
          <div className="grid md:grid-cols-2 grid-cols-4 gap-4">
            {imageUrls.map((url, index) => (
              <LoopDiv imageUrl={url} key={index} />
            ))}
            <div className="card w-full bg-gradient22 ">
              <PieChartMui />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LiveDashboard;
