import React, { useState } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import BASE_URL from "apiConfig";
import ResponsiveAppBar from "../../components/Navbar";
import {
  LoginHeader,
  Img,
  LoginText,
  LoginInput,
  LoginButton,
  Footer,
  VideoPlayer,
  Carousel,
} from "components";

function NewLogin() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [userPrivileges, setUserPrivileges] = useState([]);
  const containerStyle: React.CSSProperties = {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 39px', // Negative margin to offset padding in columns
  };

  const columnStyle: React.CSSProperties = {
    flex: '1 0 30%', // 50% width for a two-column layout
    boxSizing: 'border-box',
    padding: '0 10px', // Adjust padding as needed
  };
  const textContainerStyle: React.CSSProperties = {
    marginBottom: '26px', // Adjust the desired padding between text elements
  };


  const redirectToDesktopTwenty = () => {
    // Implement your custom redirection logic for 'alpr'
    // For example:
    window.location.href = '/desktoptwenty';
  };
  
  const redirectToDesktopTwentyOne = () => {
    // Implement your custom redirection logic for 'Human detection'
    // For example:
    window.location.href = '/desktoptwentyone';
  };
  
  const redirectToDesktopTwentyThree = () => {
    // Implement your custom redirection logic for 'helmet detection'
    // For example:
    window.location.href = '/desktoptwentythree';
  };
  
  const redirectToDesktopTwentyTwo = () => {
    // Implement your custom redirection logic for 'Cup detection'
    // For example:
    window.location.href = '/DesktopTwentyTwo';
  };
  const handleSignIn = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(`${BASE_URL}/signin/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: username,
          password: password,
        }),
      });
    
      if (response.ok) {
        const userData = await response.json();
        console.log("Post Success - UserData:", userData);
        
        const userPrivileges = userData.privilege || [];
        localStorage.setItem('userPrivileges', JSON.stringify(userPrivileges));
        
        // Log stored user privileges
        const storedUserPrivileges = localStorage.getItem('userPrivileges');
        console.log('Stored User Privileges:', storedUserPrivileges);
        
        setUserPrivileges(userPrivileges);
        
        // Redirect based on user privileges
        // if (userPrivileges.includes('alpr')) {
        //   redirectToDesktopTwenty();
        // } else if (userPrivileges.includes('Human detection')) {
        //   redirectToDesktopTwentyOne();
        // } else if (userPrivileges.includes('helmet detection')) {
        //   redirectToDesktopTwentyThree();
        // } else if (userPrivileges.includes('Cup detection')) {
        //   redirectToDesktopTwentyTwo();
        // }
      }
    } catch (error) {
      // Handle any exceptions that occur within the try block
      console.error("Error:", error);
    } finally {
      // Code to execute whether there's an error or not (optional)
    }
    
      
  
      
};

  return (
    <>
      <Grid>
        <ResponsiveAppBar />
        <Grid id="home">
          <LoginHeader />

        </Grid>
        
        <div id="about" className="mx-20 md:mx-auto flex flex-row md:flex-col items-center">
  {/* Image */}
  <Img
  className="m-auto pt-2 pb-2 rounded-lg"  // Add 'rounded-lg' for a large rounded effect
  src="images/newlog.png"
  alt="productivity"
  width={600}
  height={1000}
  style={{ borderRadius: '25px' }}  // You can adjust the radius value as needed
/>

  {/* Text Content */}
  <div className=" py-[7%] text-left md:pl-10 mb-20 ml-14">
    <h6 className="text-6xl font-bold mb-10">Vbot Checklists</h6>
    <h4 className="mb-8 text-2xl font-bold">
      Run your use case checklists on your cameras across multiple locations in minutes.
    </h4>

    <ul className="py-[7%] list-disc pl-6 space-y-8" style={{ fontSize: '22px' }}>
      <li><strong>All-in-One Solution:</strong> vBot is an all-in-one solution that offers various functionalities.</li>
      <li><strong>Cross-Platform:</strong> It is designed to work seamlessly across different platforms.</li>
      <li><strong>Functionality Integration:</strong> It combines Video Management, Surveillance, and Video Analytics services into a single solution.</li>
      <li><strong>Simplified Management:</strong> vBot simplifies the management of cameras and Network Video Recorders (NVRs) across multiple locations.</li>
      <li><strong>Unified Control:</strong> It provides a unified space or interface for controlling cameras and NVRs.</li>
      <li><strong>Scalability:</strong> Users can easily add cameras and use cases as needed.</li>
      <li><strong>User-Friendly Interface:</strong> The interface is designed to be user-friendly for ease of use.</li>
      <li><strong>Alert Systems:</strong> It offers configurable alert systems, which can enhance security and operational efficiency.</li>
    </ul>
  </div>
  
</div>
<div  className=" py-[1%] " style={containerStyle}>
      <div style={columnStyle}>
        <div style={textContainerStyle}>
          <h4 style={{ marginBottom: 8, fontSize: '1.8em', fontWeight: 'bold' }}>
            Assign and Manage Tasks with Your Cameras
          </h4>
          <p style={{ fontSize: '1.2em' }}>
            Every checklist consists of multiple tasks. Utilize your cameras to oversee and manage tasks across different locations.
          </p>
        </div>

        <div style={textContainerStyle}>
          <h4 style={{ marginBottom: 8, fontSize: '1.8em', fontWeight: 'bold' }}>
            Schedule and Automate Tasks
          </h4>
          <p style={{ fontSize: '1.2em' }}>
            Streamline your task management process by scheduling tasks at specific times and defining their frequency. Run detections across multiple cameras and locations with ease.
          </p>
        </div>
      </div>

      <div style={columnStyle}>
        <div style={textContainerStyle}>
          <h4 style={{ marginBottom: 8, fontSize: '1.8em', fontWeight: 'bold' }}>
            Customize Detection Areas
          </h4>
          <p style={{ fontSize: '1.2em' }}>
            Align your cameras with your specific needs. Whether it’s monitoring your billing machine, a critical area in the restaurant kitchen, or the entrance gate, create precise detection areas for detailed monitoring.
          </p>
        </div>

        <div style={textContainerStyle}>
          <h4 style={{ marginBottom: 8, fontSize: '1.8em', fontWeight: 'bold' }}>
            Enhance Team Productivity
          </h4>
          <p style={{ fontSize: '1.2em' }}>
            Boost productivity by assigning tasks to your team members. Easily distribute work among your teammates by entering their emails, and collaborate on task management.
          </p>
        </div>
      </div>
    </div>

<div id="about" className=" py-[6%] mx-20 md:mx-auto flex flex-row md:flex-col items-center">
  {/* Image */}
  <Img
    className="m-auto pt-2 pb-2 rounded-lg"
    src="images/logimg2.png"
    alt="productivity"
    width={600}
    height={1000}
    style={{ borderRadius: '25px' }} 
  />

  {/* Text Content */}
  <div className="text-left md:pl-10 mb-20 ml-14">
    <h6 className="text-6xl font-bold mb-10">VbotCam</h6>
    <h4 className="mb-8 text-2xl font-bold">
      Experience Seamless Video Surveillance with VbotCam
    </h4>

    <p className="mb-8">
      VbotCam is your trusted solution for advanced video surveillance and monitoring. With cutting-edge technology and user-friendly features, VbotCam ensures that you can keep a watchful eye on what matters most to you, whether it’s your home, business, or any other property.
    </p>

    <h4 className="mb-6 text-xl font-bold">Why Choose VbotCam?</h4>

    <ul className="py-[7%] list-disc pl-6 space-y-8" style={{ fontSize: '22px' }}>
      <li><strong>Easy Onboarding:</strong> Getting started is a breeze.</li>
      <li><strong>Monitor Anywhere:</strong> Keep an eye on your cameras from anywhere, anytime.</li>
      <li><strong>Insight on the Go:</strong> Access insights and information no matter where you are.</li>
    </ul>
  </div>
</div>



<div id="about" className=" py-[7%] mx-20 md:mx-auto flex flex-row md:flex-col items-center">
  {/* Image */}
 

  {/* Text Content */}
  <div className="text-left md:pl-10 mb-20 ml-14">
    <h6 className="text-6xl font-bold mb-10">VBot Analytics</h6>
    <h4 className="mb-8 text-2xl font-bold">
      Enhanced Security Through AI-Powered Video Analytics
    </h4>

    <p className="mb-8">
      In today’s fast-paced world, security and data-driven decision-making are paramount. VBot Analytics brings a cutting-edge solution to the table with AI-powered video analytics designed to fortify security, streamline automation, and elevate business decision-making.
    </p>

    <h4 className="mb-6 text-xl font-bold">The Power of AI-Driven Analytics</h4>

    <ul className="py-[7%] list-disc pl-6 space-y-8" style={{ fontSize: '22px' }}>
      <li><strong>Tailored for Security:</strong> VBot Analytics leverages the power of artificial intelligence to create job-specific analytics modules. These modules are finely tuned to enhance security measures, ensuring that every aspect of your environment is monitored and protected.</li>
      <li><strong>Enhanced Automation:</strong> With AI, the capabilities of VBot Analytics go beyond simple surveillance. It empowers automation by identifying patterns, anomalies, and potential threats in real-time, allowing for immediate responses and preventive measures.</li>
      <li><strong>Informed Decision-Making:</strong> Make smarter business decisions backed by data-driven insights. VBot Analytics provides a wealth of information derived from video feeds, helping you understand customer behavior, traffic patterns, and operational efficiencies.</li>
    </ul>
  </div>
  <Img
    className="m-auto pt-2 pb-2 rounded-lg"
    src="images/logimg3.png"
    alt="productivity"
    width={800}
    height={1000}
    style={{ borderRadius: '25px' }} 
  />
</div>

    <div  className=" py-[6%]" style={{ display: 'flex', justifyContent: 'space-between', padding: '0 20px' }}>
    <div style={{ flex: '1', margin: '0 20px', display: 'flex', flexDirection: 'column' }}>
          <div style={textContainerStyle}>
            <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/licenceplate.svg"  // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
                <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
                  License Plate Recognition
                </h4>
                <p style={{ fontSize: '1.4em' }}>
                  Vehicle license plate recognition, car make, model, colour, and direction recognition.
                </p>
              </div>
            </div>
          </div>
          


        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/vehicletraffic.svg"  // Replace with the actual path to your image
                alt="images/vehicletraffic.svg"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Vehicle Traffic
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Vehicle traffic intensity statistics calculation. Indicates the
            number of vehicles.
          </p>
        </div>
        </div>
        </div>

        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '130px' }}>
              <Img
                src="images/crowddetection.svg" // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Crowd Detection
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Detects a crowd of people. Indicates the number of people.
          </p>
        </div>
        </div>
        </div>

        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/firedetection.svg" // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Smoke and Fire Detection
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Early detection of smoke and fire.
          </p>
        </div>
        </div>
        </div>
      </div>

      <div style={{ flex: '1', margin: '0 20px' }}>
        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/facerecoginition.svg" // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Face Recognition
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Face detection and comparison with predefined lists of faces. Estimates
            the age and gender of the detected person.
          </p>
        </div>
        </div>
        </div>

        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/proplecounting.svg"  // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            People Counting
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Human traffic intensity statistics calculation. Indicates the number
            of people in the room.
          </p>
        </div>
        </div>
        </div>

        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '180px' }}>
              <Img
                src="images/metadata.svg"  // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Metadata Search
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Search by attributes: color of clothes, bags, hair, hat, gender, and
            age.
          </p>
        </div>
        </div>
        </div>

        <div style={textContainerStyle}>
        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '150px' }}>
              <Img
                src="images/posedetection.svg"  // Replace with the actual path to your image
                alt="License Plate Recognition"
                className="mr-3"  // Add margin-right to create space between image and text
                width={100}  // Set the width of the image as needed
                height={100}  // Set the height of the image as needed
              />
              <div>
          <h4 style={{ marginBottom: '35px', fontSize: '2em', fontWeight: 'bold' }}>
            Pose Detection
          </h4>
          <p style={{ fontSize: '1.4em' }}>
            Detects walking, standing, sitting, and laying people.
          </p>
        </div>
        </div>
        </div>
      </div>
    </div>


{/* 
        <Grid id="video">
          <Container>
            <VideoPlayer
              videoSource="images/login/video.mp4"
              poster="images/login/poster.png"
            />
          </Container>
        </Grid>
        <Grid>
          <Container>
            <Box sx={{ flexGrow: 1, margin: "8rem 0" }}>
              <Grid
                container
                spacing={{ xs: 2, md: 3 }}
                columns={{ xs: 4, sm: 8, md: 12 }}
              >
                <Grid item xs={12} sm={4} md={4}>
                  <Img
                    className="m-auto pt-16 pb-4"
                    src="images/login/productivity.svg"
                    alt="productivity"
                  />
                  <Box sx={{ justifyContent: "center", display: "grid" }}>
                    <Typography
                      variant="h4"
                      sx={{
                        textAlign: "center",
                        fontSize: "3.5rem",
                        fontWeight: "1000",
                        display: "flex",
                      }}
                    >
                      <LoginText
                        className="text-[55px] leading-10"
                        size="txtMontserratExtraBold90"
                      >
                        01
                      </LoginText>

                      <LoginText
                        className="text-lg leading-6 ml-4 -mt-1"
                        size="txtMontserratBold20"
                      >
                        Increased <br /> Productivity
                      </LoginText>
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={4} md={4}>
                  <Img
                    className="m-auto pt-16 pb-4"
                    src="images/login/flexibility.svg"
                    alt="flexibility"
                  />
                  <Box sx={{ justifyContent: "center", display: "grid" }}>
                    <Typography
                      variant="h4"
                      sx={{
                        textAlign: "center",
                        fontSize: "3.5rem",
                        fontWeight: "1000",
                        display: "flex",
                      }}
                    >
                      <LoginText
                        className="text-[55px] leading-10"
                        size="txtMontserratExtraBold90"
                      >
                        02
                      </LoginText>
                      <LoginText
                        className="text-lg leading-6 ml-4 -mt-1"
                        size="txtMontserratBold20"
                      >
                        Greater <br /> Flexibility
                      </LoginText>
                    </Typography>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={4} md={4}>
                  <Img
                    className="m-auto pt-16 pb-4"
                    src="images/login/transparency.svg"
                    alt="transparency"
                  />
                  <Box sx={{ justifyContent: "center", display: "grid" }}>
                    <Typography
                      variant="h4"
                      sx={{
                        textAlign: "center",
                        fontSize: "3.5rem",
                        fontWeight: "1000",
                        display: "flex",
                      }}
                    >
                      <LoginText
                        className="text-[55px] leading-10"
                        size="txtMontserratExtraBold90"
                      >
                        03
                      </LoginText>
                      <LoginText
                        className="text-lg leading-6 ml-4 -mt-1"
                        size="txtMontserratBold20"
                      >
                        Improved <br /> Transparency
                      </LoginText>
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </Grid> */}
        <Grid container>
          <Grid
            item
            xs={12}
            md={6}
            sx={{ padding: "3rem" }}
            style={{
              background:
                "transparent linear-gradient(138deg, #001fd6 0%, #19317B 100%) 0% 0% no-repeat padding-box ",
            }}
          >
            <Box
              sx={{
                height: "28vh",
                display: "grid",
                margin: "3rem 0",
              }}
            >
              <LoginText
                className="text-white my-5 text-[45px] leading-[1] h-50"
                size="txtMontserratExtraBold90"
              >
                MAKE YOUR <br /> ESCALATIONS QUICKER
                <LoginText
                  className="text-[16px] text-white leading-6 mt-3"
                  size="txtMontserratBold20"
                >
                  Download the App
                </LoginText>
              </LoginText>
            </Box>
            <Box>
              <LoginText
                className="text-white leading-7 text-[16px]"
                size="txtMontserratLight18"
              >
                Get your App link by SMS,
                <br /> Please enter your mobile number below
              </LoginText>
            </Box>
            <Box
              className="flex"
              sx={{
                margin: "1rem 0",
              }}
            >
              <LoginInput
                placeholder="Mobile number"
                className="font-semibold placeholder:text-white-A700 w-full"
                wrapClassName="w-50 text-white"
              />
              <LoginButton className="w-40 font-bold ml-10">
                Get APP
              </LoginButton>
            </Box>
            <Box sx={{ marginTop: "3rem", paddingBottom: "3rem" }}>
              <LoginText
                className="text-white leading-7 text-[16px] mb-2"
                size="txtMontserratSemiBold25"
              >
                Available on
              </LoginText>
              <Box>
                <Grid container>
                  <Grid item xs={6} sm={6} md={4}>
                    <Img
                      className="w-[75%]"
                      src="images/login/playstore.png"
                      alt="playstore"
                    />
                  </Grid>
                  <Grid item xs={6} sm={6} md={4}>
                    <Img
                      className="w-[75%]"
                      src="images/login/appstore.png"
                      alt="appstore"
                    />
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} md={6}>
            <Img
              className="h-[100%] object-cover"
              src="images/login/app.png"
              alt="productivity"
            />
          </Grid>
        </Grid>
        <Box id="contact">
          <Grid
            container
            className="sm:p-0"
            sx={{
              backgroundImage: "url('images/login/form-bg.png')",
              backgroundSize: "cover",
              height: "100vh",
              padding: "10rem 5rem",
            }}
          >
            <Grid item sm={12} md={6}>
              <LoginText
                className="text-lg text-white leading-[1] text-[56px] mb-4"
                size="txtMontserratExtraBold152"
              >
                ANY
                <br /> QUERIES?
              </LoginText>
              <LoginText
                className="text-white leading-[1] text-[14px] pt-2"
                size="txtMontserratBold20"
              >
                Get in touch with us
              </LoginText>
              <Box sx={{ margin: "1rem 0" }}>
                <Grid container>
                  <Grid item md={4}>
                    <LoginText
                      className="text-white leading-[1] text-[14px] mb-3"
                      size="txtMontserratRegular18"
                    >
                      +91 9947217142
                    </LoginText>
                  </Grid>
                  <Grid item md={4}>
                    <LoginText
                      className="text-white leading-[1] text-[14px] mb-3"
                      size="txtMontserratRegular18"
                    >
                      vibot@gmail.com
                    </LoginText>
                  </Grid>
                  <Grid item md={12}>
                    <LoginText
                      className="text-white leading-[1] text-[14px] mb-2"
                      size="txtMontserratRegular18"
                    >
                      
                    </LoginText>
                  </Grid>
                </Grid>
              </Box>
              <Box>
                <LoginInput
                  type="email"
                  placeholder="Your email"
                  className="font-semibold placeholder:text-white-A700 w-full"
                  wrapClassName="my-3 text-white"
                />
                <LoginInput
                  placeholder="Your message"
                  className="font-semibold placeholder:text-white-A700 w-full"
                  wrapClassName="my-3 text-white"
                />
                <LoginButton className="text-dark font-bold w-[25%] block ml-auto py-2">
                  Send
                </LoginButton>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Box>
          <Footer />
        </Box>
      </Grid>

      {/* <Grid container component="main" sx={{ height: "100vh" }}>
      <CssBaseline />
      <Grid
        item
        xs={false}
        sm={4}
        md={7}
        sx={{
          backgroundImage:
            "url(https://source.unsplash.com/random?technology)",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <Box
          sx={{
            my: 18,
            mx: 4,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSignIn} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, background: "#000" }}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Typography variant="body2">
                  Forgot password?
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body2">
                  {"Don't have an account? Sign Up"}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Grid>
    </Grid> */}
    </>
  );
}

export default NewLogin;
