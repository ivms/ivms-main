import { Img } from 'components/Img';
import React from 'react';

const Settings = () => {
  return (
    <div>
      <Img
        src="images/cms.png"
        alt="settings"
        style={{ width: '70%', height: 'auto', marginRight: '90px', marginLeft: '300px' }}
      />
    </div>
  );
}

export default Settings;