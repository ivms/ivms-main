import React, { useState, useEffect, useRef } from 'react';

function WebSocketComponent() {
  const [messages, setMessages] = useState([]);
  const [cupCount, setCupCount] = useState('');
  const [receivedFrame, setReceivedFrame] = useState(null);
  const socketRef = useRef(null);
  const [hasReceivedResponse, setHasReceivedResponse] = useState(false);

  useEffect(() => {
    socketRef.current = new WebSocket('ws://65.108.149.173:8008/ws/cup-count/');

    socketRef.current.onopen = function () {
      console.log('Socket connected!');
      sendStartMessage();
    };

    socketRef.current.onmessage = function(event) {
      const receivedMessage = JSON.parse(event.data);
      setMessages(prevMessages => [...prevMessages, receivedMessage]);

      if (receivedMessage.frame && receivedMessage.cup_count && !hasReceivedResponse) {
        const { frame, cup_count } = receivedMessage;
        setCupCount(`Cup Count: ${cup_count}`);
        setReceivedFrame(convertBase64ToImage(frame));
        setHasReceivedResponse(true);
      }
    };

    return () => {
      socketRef.current.close();
    };
  }, [hasReceivedResponse]);

  const sendStartMessage = () => {
    const startMessage = {
      type: 'start'
    };
    socketRef.current.send(JSON.stringify(startMessage));
  };

  function convertBase64ToImage(base64Str) {
    return <img src={`data:image/jpeg;base64, ${base64Str}`} alt="Received Frame" />;
  }

  return (
    <div>
      <h2>Server Response:</h2>
      <p>{cupCount}</p>
      <div>{receivedFrame}</div>
    </div>
  );
}

export default WebSocketComponent;
